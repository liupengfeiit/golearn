package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"os"
	"sync"
)

// http://jzsd.hncourt.gov.cn/oc9ri
// 1 http://jzsd.hncourt.gov.cn/ovk39
// 2 http://jzsd.hncourt.gov.cn/ovleu
// 3 http://jzsd.hncourt.gov.cn/ovlve
// 4 http://jzsd.hncourt.gov.cn/ovcqe
// 5 http://jzsd.hncourt.gov.cn/ovcmj
// 6 http://jzsd.hncourt.gov.cn/ovvgg
// 7 http://jzsd.hncourt.gov.cn/ovvve
// 8 http://jzsd.hncourt.gov.cn/ovv53
// 9 http://jzsd.hncourt.gov.cn/ovnqx
// 10 http://jzsd.hncourt.gov.cn/ovmqf
// 11 http://jzsd.hncourt.gov.cn/ov0g1
// 12 http://jzsd.hncourt.gov.cn/ov0g3
// 13 http://jzsd.hncourt.gov.cn/ov1hy
// 14 http://jzsd.hncourt.gov.cn/ov1ha

// 1. 解析资源描述信息
// 2. 创建文件
// 3. 下载资源
// 4. 检查文件大小
// 5. 重试下载

type henanResource struct {
	name string // 文件名称
	url  string // 文件路径
}

func parseResource(url string) ([]*henanResource, error) {
	resp, err := http.Get(url)
	defer resp.Body.Close()
	if err != nil {
		return nil, errors.New(fmt.Sprintf("请求失败!!!, err:%s, url:%s \n", err.Error(), url))
	}
	jsonStr, _ := io.ReadAll(resp.Body)
	var jsonMap map[string]interface{}
	err = json.Unmarshal(jsonStr, &jsonMap)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("反序列化失败!!!, err:%s, jsonStr:%s \n", err.Error(), string(jsonStr)))
	}
	resultMap, ok := jsonMap["result"].(map[string]interface{})
	if !ok {
		return nil, errors.New(fmt.Sprintf("反序列化失败!!! 解析 result属性失败,jsonStr:%s \n", string(jsonStr)))
	}
	nodes, ok := resultMap["wsxx"].([]interface{})
	if !ok {
		return nil, errors.New(fmt.Sprintf("反序列化失败!!! 解析 result.wsxx属性失败,jsonStr:%s \n", string(jsonStr)))
	}
	result := make([]*henanResource, len(nodes))
	for i, node := range nodes {
		no := node.(map[string]interface{})
		url := no["fileUrl"].(string)
		url = fmt.Sprintf("http://sfsd.hncourt.gov.cn:8081//lookfile?url=%s", url)
		name := no["dispalyName"].(string)
		result[i] = &henanResource{
			url:  url,
			name: name,
		}
		fmt.Printf("【解析资源】 i: %d name: %s, url: %s \n", i, name, url)
	}
	return result, nil
}

func download(henan *henanResource, filePath string, retry int) {
	imgResp, _ := http.Get(henan.url)
	buf, _ := io.ReadAll(imgResp.Body)
	os.WriteFile(filePath+henan.name, buf, fs.ModePerm)
	fi, _ := os.Stat(filePath + henan.name)
	if fi.Size() == 0 {
		if retry > 0 {
			fmt.Printf("【下载资源】下载资源失败,重试次数【%d】, name: %s, filePath: %s, url: %s \n", retry, henan.name, filePath+henan.name, henan.url)
			retry--
			download(henan, filePath, retry)
		} else {
			fmt.Printf("【下载资源】下载资源失败!!!, name: %s, filePath: %s, url: %s \n", henan.name, filePath+henan.name, henan.url)
		}
	} else {
		fmt.Printf("【下载资源】下载资源成功, name: %s, filePath: %s, url: %s \n", henan.name, filePath+henan.name, henan.url)
	}
}

func parallelDownLoad(url string, path string, retry int) {
	arr, err := parseResource(url)
	if err != nil {
		fmt.Println(err)
		return
	}
	os.Mkdir(path, fs.ModePerm)

	waitGroup := sync.WaitGroup{}
	c := make(chan int, 10)
	for _, resource := range arr {
		waitGroup.Add(1)
		c <- 1
		go func(henan *henanResource, filePath string, retry int) {
			download(henan, filePath, retry)
			<-c
			waitGroup.Done()
		}(resource, path, retry)
	}
	waitGroup.Wait()
}

func main() {
	fs := []struct {
		url   string
		path  string
		retry int
	}{
		{
			url:   "http://222.137.29.39:8000/extranetZero/api/getSdWsList?taskid=f078c2beff7249b98c2eb28369189baf&ajid=a45c4bcbe59d4ab2911e33d63dc233d6",
			path:  "/Users/liupengfei6/Downloads/fpb-down/",
			retry: 3,
		},
	}

	for _, f := range fs {
		parallelDownLoad(f.url, f.path, f.retry)
	}
}
