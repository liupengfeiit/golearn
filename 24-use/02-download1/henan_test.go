package main

import "testing"

func TestParseResource(t *testing.T) {
	parseResource("http://222.137.29.39:8000/extranetZero/api/getSdWsList?taskid=63253fae9ee94f0297875dabe4d728d1&ajid=a45c4bcbe59d4ab2911e33d63dc233d6")
}

func TestDownload(t *testing.T) {
	download(&henanResource{
		url:  "http://sfsd.hncourt.gov.cn:8081//lookfile?url=http://143.0.14.200:8082/group1/M02/CB/30/jwAO4GN3CYmAdW3jAA6dbtQkd80620.jpg",
		name: "20221116-70355.jpg",
	}, "/Users/liupengfei6/Downloads/fpb-down/", 3)
}
