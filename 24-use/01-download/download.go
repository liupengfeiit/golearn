package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"sync"
)

// http://jzsd.hncourt.gov.cn/oc9ri
// 1 http://jzsd.hncourt.gov.cn/ovk39
// 2 http://jzsd.hncourt.gov.cn/ovleu
// 3 http://jzsd.hncourt.gov.cn/ovlve
// 4 http://jzsd.hncourt.gov.cn/ovcqe
// 5 http://jzsd.hncourt.gov.cn/ovcmj
// 6 http://jzsd.hncourt.gov.cn/ovvgg
// 7 http://jzsd.hncourt.gov.cn/ovvve
// 8 http://jzsd.hncourt.gov.cn/ovv53
// 9 http://jzsd.hncourt.gov.cn/ovnqx
// 10 http://jzsd.hncourt.gov.cn/ovmqf
// 11 http://jzsd.hncourt.gov.cn/ov0g1
// 12 http://jzsd.hncourt.gov.cn/ov0g3
// 13 http://jzsd.hncourt.gov.cn/ov1hy
// 14 http://jzsd.hncourt.gov.cn/ov1ha

func main() {
	resp, _ := http.Get("http://222.137.29.39:8000/extranetZero/api/getSdWsList?taskid=63253fae9ee94f0297875dabe4d728d1&ajid=a45c4bcbe59d4ab2911e33d63dc233d6")
	// resp, _ := http.Post("http://222.137.29.39:8000/extranetZero/api/getSdWsList?taskid=7f4bb145d05c4e549328f8e0ac17afcc&ajid=a45c4bcbe59d4ab2911e33d63dc233d6", "application/json", bytes.NewBuffer([]byte("")))
	defer resp.Body.Close()
	buf := make([]byte, 2048)
	jsonStr := ""
	for {
		n, err := resp.Body.Read(buf)
		if err == io.EOF {
			break
		}
		jsonStr += string(buf[:n])
	}

	fmt.Println(jsonStr)
	fmt.Println("-----------------------------")
	var jsonMap map[string]interface{}
	json.Unmarshal([]byte(jsonStr), &jsonMap)

	resultMap, _ := jsonMap["result"].(map[string]interface{})
	nodes := resultMap["wsxx"].([]interface{})

	waitGroup := sync.WaitGroup{}
	waitGroup.Add(len(nodes))
	for i, node := range nodes {
		go func(i int, node interface{}) {
			no := node.(map[string]interface{})
			url := no["fileUrl"].(string)
			name := no["dispalyName"].(string)
			fmt.Printf("i: %d name: %s,  url: http://sfsd.hncourt.gov.cn:8081//lookfile?url=%s \n", i, name, url)
			url = fmt.Sprintf("http://sfsd.hncourt.gov.cn:8081//lookfile?url=%s", url)
			downLoad("/Users/liupengfei6/Downloads/fpb-down/", name, url)
			defer waitGroup.Done()
		}(i, node)
	}
	waitGroup.Wait()
}

func downLoad(filePath, name, url string) {
	imgResp, err := http.Get(url)
	fmt.Printf("%s , %s, status:%s, err: %+v \n", name, url, imgResp.Status, err)
	if err != nil && imgResp.StatusCode != 200 {
		for i := 0; i < 3; i++ {
			imgResp, err = http.Get(url)
			if imgResp.StatusCode == 200 {
				break
			} else {
				fmt.Printf("%s 下载失败\n", url)
			}
		}
	}

	f, _ := os.Create(filePath + name)
	defer f.Close()
	imgBuf := make([]byte, 2048)
	for {
		n, err := imgResp.Body.Read(imgBuf)
		if err == io.EOF {
			break
		}
		w := bufio.NewWriter(f)
		w.Write(imgBuf[:n])
		w.Flush()
	}

	fi, _ := f.Stat()
	if fi.Size() == 0 {
		fmt.Printf("file is empty!!! %s , %s, status:%s, err: %+v \n", name, url, imgResp.Status, err)
	}
}
