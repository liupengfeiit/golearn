package _1_proxy

// Subject 抽象主体接口，通过接口声明真实主题和代理对象实现的业务方法
type Subject interface {
	Do() string
}

// RealSubject 真实主题
type RealSubject struct {
}

func (RealSubject) Do() string {
	return "real"
}

// Proxy 代理， 代理实现被代理类的接口， 持有被代理类的实例
type Proxy struct {
	real RealSubject
}

func (p *Proxy) Do() string {
	var res string
	res += "pre:"
	res += p.real.Do()
	res += ":after"
	return res
}
