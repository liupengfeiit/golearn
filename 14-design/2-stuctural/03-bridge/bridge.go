package _3_bridge

import "fmt"

// AbstractMessage 抽象化角色 该角色是桥
type AbstractMessage interface {
	SendMessage(text, to string)
}

// MessageImplementer 实现化（Implementor）角色：定义实现化角色的接口，供扩展抽象化角色调用。
type MessageImplementer interface {
	Send(text, to string)
}

// MessageSMS 具体实现化（Concrete Implementor）角色：给出实现化角色接口的具体实现。
type MessageSMS struct {
}

func ViaSMS() MessageImplementer {
	return &MessageSMS{}
}

func (m *MessageSMS) Send(text, to string) {
	fmt.Printf("send %s to %s via SMS", text, to)
}

type MessageEmail struct {
}

func ViaEmail() MessageImplementer {
	return &MessageEmail{}
}

func (m *MessageEmail) Send(text, to string) {
	fmt.Printf("send %s to %s via Email", text, to)
}

// CommonMessage 扩展抽象化（Refined Abstraction）角色：是抽象化角色的子类，实现父类中的业务方法，并通过组合关系调用实现化角色中的业务方法。
type CommonMessage struct {
	method MessageImplementer
}

func NewCommonMessage(method MessageImplementer) *CommonMessage {
	return &CommonMessage{method: method}
}

func (c *CommonMessage) SendMessage(text, to string) {
	c.method.Send(text, to)
}

// UrgencyMessage 扩展抽象化（Refined Abstraction）角色：是抽象化角色的子类，实现父类中的业务方法，并通过组合关系调用实现化角色中的业务方法。
type UrgencyMessage struct {
	method MessageImplementer
}

func NewUrgencyMessage(method MessageImplementer) *UrgencyMessage {
	return &UrgencyMessage{method: method}
}

func (u *UrgencyMessage) SendMessage(text, to string) {
	u.method.Send(fmt.Sprintf("[Urgency] %s", text), to)
}
