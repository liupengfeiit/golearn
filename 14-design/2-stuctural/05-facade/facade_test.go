package _5_facade

import "testing"

func TestFacadeAPI(t *testing.T) {
	api := NewAPI()
	ret := api.Test()
	const expect = "A module running\nB module running"
	if ret != expect {
		t.Fatalf("expect %s, return %s", expect, ret)
	}
}
