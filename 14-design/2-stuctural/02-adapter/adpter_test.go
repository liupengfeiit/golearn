package _2_adapter

import "testing"

func TestAdapter(t *testing.T) {
	adaptee := NewAdaptee()
	adapter := NewAdapter(adaptee)
	if adapter.Request() != "adaptee method" {
		t.Fatal()
	}
}
