package _2_adapter

// Target 适配的目标接口
type Target interface {
	Request() string
}

// Adaptee 被适配的目标接口， 现存的接口信息
type Adaptee interface {
	SpecificRequest() string
}

func NewAdaptee() Adaptee {
	return &adapteeImpl{}
}

// adapteeImpl 是被适配的目标类
type adapteeImpl struct {
}

func (*adapteeImpl) SpecificRequest() string {
	return "adaptee method"
}

func NewAdapter(adaptee Adaptee) Target {
	return &adapter{adaptee}
}

// Adapter 是转换 Adaptee为Target接口的适配器
type adapter struct {
	Adaptee
}

// 访问Request()即访问被适配者的SpecificRequest()
func (a *adapter) Request() string {
	return a.SpecificRequest()
}
