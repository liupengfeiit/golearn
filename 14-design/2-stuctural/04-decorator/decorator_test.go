package _4_decorator

import "fmt"

func ExampleDecorator() {
	var c Component = &ConcreteComponent{}
	c = WarpMulDecorator(c, 10)
	c = WarpAddDecorator(c, 8)
	res := c.Calc()
	fmt.Printf("res %d\n", res)
	// Output:
	// res 8
}

func ExampleDecorator1() {
	var c Component
	c = &ConcreteComponent{}
	c = WarpAddDecorator(c, 10)
	c = WarpMulDecorator(c, 8)
	res := c.Calc()
	fmt.Printf("res %d\n", res)
	// Output:
	// res 80
}
