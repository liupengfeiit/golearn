package _4_decorator

// Component 抽象构件角色，定义一个抽象接口以规范准备接收附加责任的对象
type Component interface {
	Calc() int
}

// ConcreteComponent  具体构件
type ConcreteComponent struct {
}

func (*ConcreteComponent) Calc() int {
	return 0
}

// MulDecorator  持有抽象构件角色的实例， 包装Component 乘于
type MulDecorator struct {
	Component
	num int
}

func WarpMulDecorator(c Component, num int) Component {
	return &MulDecorator{
		Component: c,
		num:       num,
	}
}

func (m *MulDecorator) Calc() int {
	return m.Component.Calc() * m.num
}

type AddDecorator struct {
	Component
	num int
}

func WarpAddDecorator(c Component, num int) Component {
	return &AddDecorator{
		Component: c,
		num:       num,
	}
}

func (d *AddDecorator) Calc() int {
	return d.Component.Calc() + d.num
}
