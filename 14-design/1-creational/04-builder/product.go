package builder

import "fmt"

// 产品
type Product struct {
	partA string
	partB string
	partC string
}

func (p *Product) setPartA(partA string) {
	p.partA = partA
}

func (p *Product) setPartB(partB string) {
	p.partB = partB
}

func (p *Product) setPartC(partC string) {
	p.partC = partC
}

func (p *Product) Show() {
	fmt.Printf("partA:%s\n partB:%s\n partC:%s\n", p.partA, p.partB, p.partC)
}

type Builder interface {
	Create() *Product
}
