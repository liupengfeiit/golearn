package builder

import "testing"

func TestDirector_Construct(t *testing.T) {
	director := Director{&FirstBuilder{}}
	product1 := director.Construct()
	product1.Show()

	director.b = &TwoBuilder{}
	product2 := director.Construct()
	product2.Show()
}
