package builder

type FirstBuilder struct {
}

func (f *FirstBuilder) Create() *Product {
	p := Product{}
	p.setPartA("first builder set partA...")
	p.setPartB("first builder set partB...")
	p.setPartC("first builder set partC...")
	return &p
}
