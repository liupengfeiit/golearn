package builder

type TwoBuilder struct {
}

func (f *TwoBuilder) Create() *Product {
	p := Product{}
	p.setPartA("two builder set partA...")
	p.setPartB("two builder set partB...")
	p.setPartC("two builder set partC...")
	return &p
}
