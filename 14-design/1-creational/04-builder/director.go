package builder

// 指挥者，用于创建产品

type Director struct {
	b Builder
}

func (d *Director) Construct() *Product {
	return d.b.Create()
}
