package singleton

func IncrementAge() {
	p := GetInstance()
	p.IncrementAge()
}
