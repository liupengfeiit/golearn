package singleton

import "sync"

// 定义全局内部变量p, 由init 函数来完成初始化, 由sync.Once 保证只被执行一次
// 对于单例对象注意操作时保证安全性
var (
	p    *Pet
	once sync.Once
)

func init() {
	once.Do(func() {
		p = &Pet{}
	})
}

func GetInstance() *Pet {
	return p
}

type Pet struct {
	name string
	age  int
	mux  sync.RWMutex
}

func (p *Pet) SetName(n string) {
	p.mux.Lock()         // 写锁
	defer p.mux.Unlock() // 写解锁
	p.name = n
}

func (p *Pet) GetName() string {
	p.mux.RLock()
	defer p.mux.RUnlock()
	return p.name
}

func (p *Pet) IncrementAge() {
	p.mux.Lock()
	defer p.mux.Unlock()
	p.age++
}

func (p *Pet) GetAge() int {
	p.mux.RLock()
	defer p.mux.RUnlock()
	return p.age
}
