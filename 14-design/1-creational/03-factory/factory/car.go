package factory

import "fmt"

type Car interface {
	run()
}

type Audi struct {
}

type Benz struct {
}

func (car *Audi) run() {
	fmt.Println("audi running")
}

func (car *Benz) run() {
	fmt.Println("benz running")
}
