package factory

import (
	"errors"
	"fmt"
	"sync"
)

type CarFactory interface {
	produce() Car
}

type AudiFactory struct {
}

type BenzFactory struct {
}

func (factory *AudiFactory) produce() Car {
	return &Audi{}
}

func (factory *BenzFactory) produce() Car {
	return &Benz{}
}

const (
	AUDI = iota
	BENZ
)

// 每次调用都会创建一个CarFactory
func GetCarFactory(t int) (CarFactory, error) {
	switch t {
	case AUDI:
		return &AudiFactory{}, nil
	case BENZ:
		return &BenzFactory{}, nil
	default:
		return nil, errors.New(fmt.Sprintf("%d 不能识别,创建factory失败", t))
	}
}

var (
	audiFacotry *AudiFactory
	benzFacotry *BenzFactory
	once        sync.Once
)

func init() {
	once.Do(func() {
		audiFacotry = &AudiFactory{}
		benzFacotry = &BenzFactory{}
	})
}

func GetCarFacotryInstance(t int) (CarFactory, error) {
	switch t {
	case AUDI:
		return audiFacotry, nil
	case BENZ:
		return benzFacotry, nil
	default:
		return nil, errors.New(fmt.Sprintf("%d 不能识别,创建factory失败", t))
	}
}
