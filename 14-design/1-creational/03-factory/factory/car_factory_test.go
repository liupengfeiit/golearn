package factory

import (
	"fmt"
	"testing"
)

func TestGetCarFactory(t *testing.T) {
	factory, _ := GetCarFactory(AUDI)
	car := factory.produce()
	car.run()
	factory, _ = GetCarFactory(BENZ)
	car = factory.produce()
	car.run()
	_, error := GetCarFactory(22)
	if error != nil {
		fmt.Println(error.Error())
	}
}

func TestGetCarFacotryInstance(t *testing.T) {
	factory, _ := GetCarFacotryInstance(AUDI)
	factory1, _ := GetCarFactory(AUDI)
	fmt.Println(factory == factory1)
	fmt.Printf("%p\n", factory) // 指针地址相同
	fmt.Printf("%p\n", factory1)
}
