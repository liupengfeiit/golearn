package abstract

import "fmt"

// OrderMainDAO 主订单DAO
type OrderMainDAO interface {
	SaveOrderMain()
}

// OrderDetailDAO 子订单DAO
type OrderDetailDAO interface {
	SaveOrderDetail()
}

// DAOFactory DAO抽象工厂接口
type DAOFactory interface {
	CreateOrderMainDAO() OrderMainDAO
	CreateOrderDetailDAO() OrderDetailDAO
}

// RDBMainDAO 为关系型数据库的OrderMainDAO实现
type RDBMainDAO struct {
}

func (*RDBMainDAO) SaveOrderMain() {
	fmt.Println("rdb main order save")
}

// RDBDetailDAO 为关系型数据库的OrderDetailDAO实现
type RDBDetailDAO struct {
}

func (*RDBDetailDAO) SaveOrderDetail() {
	fmt.Println("rdb detail order save")
}

// RDBDAOFactory 是RDB抽象工厂实现
type RDBDAOFactory struct {
}

func (*RDBDAOFactory) CreateOrderMainDAO() OrderMainDAO {
	return &RDBMainDAO{}
}

func (*RDBDAOFactory) CreateOrderDetailDAO() OrderDetailDAO {
	return &RDBDetailDAO{}
}

// XMLMainDAO 为xml的OrderMainDAO实现
type XMLMainDAO struct {
}

func (*XMLMainDAO) SaveOrderMain() {
	fmt.Println("xml main order save")
}

// XMLMainDAO 为xml的OrderDetailDAO实现
type XMLDetailDAO struct {
}

func (*XMLDetailDAO) SaveOrderDetail() {
	fmt.Println("xml detail order save")
}

type XMLDAOFactory struct {
}

func (*XMLDAOFactory) CreateOrderMainDAO() OrderMainDAO {
	return &XMLMainDAO{}
}

func (*XMLDAOFactory) CreateOrderDetailDAO() OrderDetailDAO {
	return &XMLDetailDAO{}
}
