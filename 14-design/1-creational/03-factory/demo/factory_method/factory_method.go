package factory_method

type Operator interface {
	SetA(int)
	SetB(int)
	Result() int
}

type OperatorFactory interface {
	Create() Operator
}

type OperatorBase struct {
	a, b int
}

func (o *OperatorBase) SetA(a int) {
	o.a = a
}

func (o *OperatorBase) SetB(b int) {
	o.b = b
}

func (o *OperatorBase) Result() int {
	panic("un impl")
}

// PlusOperator 继承 OperatorBase
type PlusOperator struct {
	*OperatorBase
}

func (p *PlusOperator) Result() int {
	return p.a + p.b
}

// PlusOperatorFactory 生产PlusOperator
type PlusOperatorFactory struct {
}

func (f *PlusOperatorFactory) Create() Operator {
	return &PlusOperator{&OperatorBase{}}
}

// MinusOperator 继承 OperatorBase
type MinusOperator struct {
	*OperatorBase
}

func (m *MinusOperator) Result() int {
	return m.a - m.b
}

// MinusOperatorFactory 生产MinusOperator
type MinusOperatorFactory struct {
}

func (m *MinusOperatorFactory) Create() Operator {
	return &MinusOperator{&OperatorBase{}}
}
