package abstract

type AbstractFactory interface {
	// ProducePhone  生产手机
	ProducePhone() PhoneProduct
	// 生产电视
	ProduceTV() TvProduct
}

type PhoneProduct interface {
	Send(msg string)
}

type TvProduct interface {
	Show(msg string)
}

const (
	HUAWEI = iota
	MI
)

func GetFactory(t int) AbstractFactory {
	switch t {
	case HUAWEI:
		return &HuaweiFactory{}
	case MI:
		return &MiFactory{}
	default:
		return nil
	}
}