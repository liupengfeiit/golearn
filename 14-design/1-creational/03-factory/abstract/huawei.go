package abstract

import "fmt"

type HuaweiFactory struct {
}

type HuaweiPhone struct {
}

type HuaweiTV struct {
}

func (h *HuaweiPhone) Send(msg string) {
	fmt.Println("huawei phone send..." + msg)
}

func (h *HuaweiTV) Show(msg string) {
	fmt.Println("huawei tv show..." + msg)
}

func (h *HuaweiFactory) ProducePhone() PhoneProduct {
	return &HuaweiPhone{}
}

func (h *HuaweiFactory) ProduceTV() TvProduct {
	return &HuaweiTV{}
}
