package abstract

import "fmt"

type MiFactory struct {
}
type MiPhone struct {
}
type MiTV struct {
}

func (m *MiPhone) Send(msg string) {
	fmt.Println("mi phone send..." + msg)
}

func (m *MiTV) Show(msg string) {
	fmt.Println("mi tv show..." + msg)
}

func (m *MiFactory) ProducePhone() PhoneProduct {
	return &MiPhone{}
}

func (m *MiFactory) ProduceTV() TvProduct {
	return &MiTV{}
}
