package abstract

import "testing"

func TestGetFactory(t *testing.T) {
	huaweiFactory := GetFactory(HUAWEI)
	miFactory := GetFactory(MI)
	phone := huaweiFactory.ProducePhone()
	phone.Send("hello")
	tv := huaweiFactory.ProduceTV()
	tv.Show("hello")
	phone = miFactory.ProducePhone()
	tv = miFactory.ProduceTV()
	phone.Send("hello1")
	tv.Show("hello1")
}
