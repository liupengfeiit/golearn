package simple

import (
	"fmt"
	"testing"
)

func TestProduce(t *testing.T) {
	t1, _ := Produce(MAO_JIAN)
	t1.drink()
	t2, _ := Produce(DA_HONG_PAO)
	t2.drink()
	_, err := Produce("h")
	if err != nil {
		fmt.Println(err.Error())
	}
}
