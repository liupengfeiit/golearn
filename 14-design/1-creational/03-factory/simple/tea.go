package simple

import (
	"errors"
	"fmt"
)

type Tea interface {
	drink()
}

type MaoJian struct {
}

type DaHongPao struct {
}

func (m *MaoJian) drink() {
	fmt.Println("喝毛尖茶")
}

func (d *DaHongPao) drink() {
	fmt.Println("喝大红袍茶")
}

const (
	MAO_JIAN    = "毛尖"
	DA_HONG_PAO = "大红袍"
)

func Produce(name string) (Tea, error) {
	if MAO_JIAN == name {
		return &MaoJian{}, nil
	} else if DA_HONG_PAO == name {
		return &DaHongPao{}, nil
	}
	return nil, errors.New(fmt.Sprintf("【%s】 不能识别,创建Tea失败!\n", name))
}
