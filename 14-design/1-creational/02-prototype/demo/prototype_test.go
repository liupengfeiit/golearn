package demo

import (
	"fmt"
	"testing"
)

var manager *PrototypeManager

type Consumer interface {
	Accept()
}

type Type1 struct {
	name string
}

func (t *Type1) Clone() Cloneable {
	// *a 代表指针的值  a代表指针地址
	// 将t指针的值赋值给tc 结构体值拷贝
	tc := *t
	fmt.Printf("%p, %p, %+v\n", t, &tc, tc)
	return &tc
}

func (t *Type1) Accept() {
	t.name = "hahah"
}

type Type2 struct {
	name string
}

func (t *Type2) Clone() Cloneable {
	tc := *t
	fmt.Printf("%+v\n", tc)
	return &tc
}

func init() {
	manager = NewPrototypeManager()
	t1 := &Type1{
		name: "type1",
	}
	manager.Set("t1", t1)
}

func TestClone(t *testing.T) {
	t1 := manager.Get("t1")
	t2 := manager.Get("t1")
	// t1, t2 地址不同，为不同对象
	if t1 == t2 {
		t.Fatal("error! get clone not working")
	}

	t3 := t1.(Consumer)
	t3.Accept()
	fmt.Printf("%+v, %+v, %p, %p \n", t1, t2, t1, t2)
}

func TestCloneFromManager(t *testing.T) {
	c := manager.Get("t1").Clone()
	// 将接口断言为 结构体类型  类型的转换
	t1 := c.(*Type1)
	if t1.name != "type1" {
		t.Fatal("error")
	}
}
