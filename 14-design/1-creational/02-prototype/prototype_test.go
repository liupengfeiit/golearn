package prototype

import (
	"fmt"
	"testing"
)

func TestConcretePrototype_Clone(t *testing.T) {
	name := "lpf"
	proto := ConcretePrototype{name: name}
	newProto := proto.Clone()
	actualName := newProto.Name()

	fmt.Println(name)
	fmt.Println(actualName)
}
