package _1_template_method

import "fmt"

type Downloader interface {
	Download(uri string)
}

// 模版类,持有抽象化的接口实例
type template struct {
	implement
	uri string
}

// 抽象模版，定义抽象的方法
type implement interface {
	download()
	save()
}

func newTemplate(impl implement) *template {
	return &template{
		implement: impl,
	}
}

func (t *template) Download(uri string) {
	t.uri = uri
	// 下载前准备工作
	fmt.Print("prepare downloading\n")
	t.implement.download()
	t.implement.save()
	// 下载完毕后的收尾工作
	fmt.Print("finish downloading\n")
}

func (t *template) save() {
	fmt.Print("default save\n")
}

// HTTPDownloader 继承 template
type HTTPDownloader struct {
	*template
}

func NewHttpDownloader() Downloader {
	downloader := &HTTPDownloader{}
	template := newTemplate(downloader)
	downloader.template = template
	return downloader
}

func (d *HTTPDownloader) save() {
	fmt.Printf("http save\n")
}

func (d *HTTPDownloader) download() {
	fmt.Printf("download %s via http\n", d.uri)
}

type FTPDownloader struct {
	*template
}

func NewFTPDownloader() Downloader {
	downloader := &FTPDownloader{}
	template := newTemplate(downloader)
	downloader.template = template
	return downloader
}

func (d *FTPDownloader) download() {
	fmt.Printf("download %s via ftp\n", d.uri)
}
