package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func main() {
	r := gin.Default()
	r.POST("/upload", func(context *gin.Context) {
		_, headers, err := context.Request.FormFile("file")
		if err != nil {
			log.Printf("Error when try to get file: %v", err)
		}

		if headers.Size > 1024*1024*2 {
			fmt.Println("文件太大了")
			return
		}

		if headers.Header.Get("Content-Type") != "image/png" {
			fmt.Println("只允许上传png图片")
			return
		}
		context.SaveUploadedFile(headers, "./temp/"+headers.Filename)
		context.String(http.StatusOK, headers.Filename)
	})
	r.Run()
}
