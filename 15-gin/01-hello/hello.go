package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	// 创建路由
	r := gin.Default()
	// 绑定路由规则，执行的函数, gin.Context封装了request和response
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "hello world")
	})
	r.POST("/po", func(context *gin.Context) {
		context.String(http.StatusOK, "post test")
	})
	// 监听端口，默认8080
	r.Run(":8080")
}
