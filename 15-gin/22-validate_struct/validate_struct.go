package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// 结构体赋值校验
// curl 'http://localhost:8080/va?age=11&name=%E6%9E%AF%E8%97%A4&birthday=2006-01-02'
// curl 'http://localhost:8080/va?age=1&name=%E6%9E%AF%E8%97%A4&birthday=2006-01-02'

type Person struct {
	Age      int       `form:"age" binding:"required,gt=10"`
	Name     string    `form:"name" binding:"required"`
	Birthday time.Time `form:"birthday" time_format:"2006-01-02" time_utc:"1"`
}

func main() {
	r := gin.Default()
	r.GET("/va", func(c *gin.Context) {
		var person Person
		if err := c.ShouldBind(&person); err != nil {
			c.String(http.StatusInternalServerError, fmt.Sprint(err))
			return
		}
		c.String(http.StatusOK, fmt.Sprintf("%#v", person))
	})
	r.Run()
}
