## router 路由拆分与注册

### DEMO

gin_demo <br>
|--- go.mod <br>
|--- go.sum <br>
|--- main.go <br>
|--- routers.go <br>


gin_demo <br>
|--- go.mod <br>
|--- go.sum <br>
|--- main.go <br>
|--- routers <br>
|----- routers.go <br>

### 路由拆分多个文件

gin_demo <br>
|--- go.mod <br>
|--- go.sum <br>
|--- main.go <br>
|--- routers <br>
|----- routers1.go <br>
|----- routers2.go <br>

### 路由拆分道不同的APP
gin_demo <br>
├── app <br>
│   ├── blog <br>
│   │   ├── handler.go <br>
│   │   └── router.go <br>
│   └── shop <br>
│       ├── handler.go <br>
│       └── router.go <br>
├── go.mod <br>
├── go.sum <br>
├── main.go <br>
└── routers <br>
└── routers.go <br>

```go
// blog
func Routers(e *gin.Engine) {
    e.GET("/post", postHandler)
    e.GET("/comment", commentHandler)
}
```

```go
// shop
func Routers(e *gin.Engine) {
    e.GET("/goods", goodsHandler)
    e.GET("/checkout", checkoutHandler)
}
```
```go
// routers
type Option func(*gin.Engine)

var options = []Option{}

// 注册app的路由配置
func Include(opts ...Option) {
    options = append(options, opts...)
}

// 初始化
func Init() *gin.Engine {
    r := gin.New()
    for _, opt := range options {
    opt(r)
    }
    return r
}
```
```go
// main
func main() {
    // 加载多个APP的路由配置
    routers.Include(shop.Routers, blog.Routers)
    // 初始化路由
    r := routers.Init()
    if err := r.Run(); err != nil {
    fmt.Println("startup service failed, err:%v\n", err)
}
}
```