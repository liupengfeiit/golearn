package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// 从路径中获取值

func main() {
	r := gin.Default()
	r.GET("/user/:name/*action", func(context *gin.Context) {
		name := context.Param("name")
		action := context.Param("action")
		fmt.Println(action)
		// 去除末尾的 /
		action = strings.Trim(action, "/")
		context.String(http.StatusOK, name+" is "+action)
	})
	r.Run(":8080")
}
