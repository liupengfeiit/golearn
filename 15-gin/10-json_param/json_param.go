package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// curl http://127.0.0.1:8000/loginJson -H 'Content-type:application/json' -d '{"user":"root", "password":"admin"}' -X POST

// Login /** 客户端 传参，后端接收并解析到结构体 */
type Login struct {
	User     string `form:"username" json:"user" uri:"user" xml:"user" binding:"required"`
	Password string `form:"password" json:"password" uri:"password" xml:"password" binding:"required"`
}

func main() {
	// 1.创建路由
	r := gin.Default()
	// Json绑定
	r.POST("loginJson", func(c *gin.Context) {
		var json Login
		if err := c.ShouldBindJSON(&json); err != nil {
			// gin.H封装了生成JSON数据的工具
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if json.User != "root" || json.Password != "admin" {
			c.JSON(http.StatusBadRequest, gin.H{"status": "304"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"status": "200"})
	})
	r.Run(":8000")
}
