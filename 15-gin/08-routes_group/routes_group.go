package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

// curl http://127.0.0.1:8000/v1/login?name=zhangsan
// curl -s "http://127.0.0.1:8000/v1/login?name=zhangsan"
// curl -X POST http://localhost:8000/v2/submit
func main() {
	r := gin.Default()
	// 路由组1，处理Get请求
	v1 := r.Group("/v1")
	// {} 是书写规范
	{
		v1.GET("/login", login)
		v1.GET("/submit", submit)
	}
	v2 := r.Group("/v2")
	{
		v2.POST("/login", login)
		v2.POST("/submit", submit)
	}
	r.Run(":8000")
}

func login(c *gin.Context) {
	name := c.DefaultQuery("name", "jack")
	c.String(http.StatusOK, fmt.Sprintf("hello %s\n", name))
}

func submit(c *gin.Context) {
	name := c.DefaultQuery("name", "lily")
	c.String(http.StatusOK, fmt.Sprintf("hello %s\n", name))
}
