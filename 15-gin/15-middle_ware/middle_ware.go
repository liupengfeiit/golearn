package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// 类似于Filter
// 所有请求都经过此中间件

func MiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		fmt.Println("filter start...")
		// 设置变量到Context的Key中，可以通过Get()获取
		c.Set("request", "filter")
		status := c.Writer.Status()
		fmt.Println("filter end ...", status)
		t1 := time.Since(t)
		fmt.Println("耗时:", t1)
	}
}

func main() {
	r := gin.Default()
	r.Use(MiddleWare())
	// {} 为了代码规范
	{
		r.GET("/ce", func(c *gin.Context) {
			req, _ := c.Get("request")
			fmt.Println("request:", req)
			c.JSON(http.StatusOK, gin.H{"request": req})
		})
	}
	r.Run()
}
