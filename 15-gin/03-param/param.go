package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	r.GET("/user", func(context *gin.Context) {
		// 指定默认值
		name := context.DefaultQuery("name", "param is nil")
		if name == "" {
			name = "name is \"\" "
		}
		context.String(http.StatusOK, fmt.Sprintf("hello %s", name))
	})
	// 默认端口8080
	r.Run()
}
