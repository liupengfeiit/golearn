package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/testdata/protoexample"
	"net/http"
)

// 数据格式的响应  json, 结构体, XML, YAML, ProtoBuf

func main() {
	r := gin.Default()
	// curl http://localhost:8000/someJson
	r.GET("/someJson", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "someJson", "status": 200})
	})
	// curl http://localhost:8000/someStruct
	r.GET("/someStruct", func(c *gin.Context) {
		var msg struct {
			Name    string
			Message string
			Number  int
		}
		msg.Name = "root"
		msg.Message = "message"
		msg.Number = 123
		c.JSON(http.StatusOK, msg)
	})
	// curl http://localhost:8000/someXml
	r.GET("/someXml", func(c *gin.Context) {
		c.XML(http.StatusOK, gin.H{"message": "abc"})
	})
	// curl http://localhost:8000/someYaml
	r.GET("/someYaml", func(c *gin.Context) {
		c.YAML(http.StatusOK, gin.H{"name": "zhangsan"})
	})
	// curl http://localhost:8000/someProtoBuf
	r.GET("/someProtoBuf", func(c *gin.Context) {
		resp := []int64{int64(1), int64(2)}
		// 定义数据
		label := "label"
		data := &protoexample.Test{
			Label: &label,
			Reps:  resp,
		}
		c.ProtoBuf(http.StatusOK, data)
	})

	r.Run(":8000")
}
