package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func MiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		fmt.Println("中间件开始执行")
		// 设置变量到Context的key中， 可以通过Get()取
		c.Set("request", "中间件")
		// 执行函数 调用下一个处理器处理
		c.Next()
		status := c.Writer.Status()
		fmt.Println("中间件执行完毕", status)
		t1 := time.Since(t)
		fmt.Println("time:", t1)
	}
}

func main() {
	r := gin.Default()
	r.Use(MiddleWare())
	// {} 为了代码规范
	{
		r.GET("ce", func(c *gin.Context) {
			req, _ := c.Get("request")
			fmt.Println("request:", req)
			c.JSON(http.StatusOK, gin.H{"request": req})
		})
	}
	r.Run()
}
