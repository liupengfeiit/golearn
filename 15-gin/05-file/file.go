package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	// 限制上传最大尺寸
	r.MaxMultipartMemory = 8 << 20
	r.POST("/upload", func(context *gin.Context) {
		file, err := context.FormFile("file")
		if err != nil {
			context.String(http.StatusInternalServerError, "上传图片出错"+err.Error())
		}
		// 文件写入到磁盘， 根路径是 ../
		context.SaveUploadedFile(file, file.Filename)
		context.String(http.StatusOK, file.Filename)
	})
	r.Run()
}
