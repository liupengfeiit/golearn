package main

import (
	"fmt"
	greetings "gitee.com/liupengfeiit/golearn/12-gomod/02-00-greeting"
	"log"
)

// 如果模块未发布的话可以使用  go mod edit -replace example.com/greetings=../greetings

// 使用greetings模块中的内容
func main() {
	log.SetPrefix("问候：")
	log.SetFlags(0)
	message, err := greetings.Hello("")
	if err != nil {
		fmt.Println(err)
		// log.Fatal(err)
	}
	//message := greetings.Hello("Gladys")
	fmt.Println(message)
	hello1()
	hello2()
}

func hello1() {
	message, err := greetings.Hello("Gladys")
	if err != nil {
		fmt.Println(err)
		// log.Fatal(err)
	}
	//message := greetings.Hello("Gladys")
	fmt.Println(message)
}

func hello2() {
	// A slice of names.
	names := []string{"Gladys", "Samantha", "Darrin"}
	// Request greeting messages for the names.
	messages, err := greetings.Hellos(names)
	if err != nil {
		log.Fatal(err)
	}
	// If no error was returned, print the returned map of
	// messages to the console.
	fmt.Println(messages)
}
