module gitee.com/liupengfeiit/golearn/12-gomod/02-01-call

go 1.16

// replace gitee.com/liupengfeiit/golearn/12-gomod/02-00-greeting => ../02-00-greetings

// require gitee.com/liupengfeiit/golearn/12-gomod/02-00-greeting v0.0.0-20220104095551-dfb11d8dfbf1

replace gitee.com/liupengfeiit/golearn/12-gomod/02-00-greeting => ../02-00-greeting

require gitee.com/liupengfeiit/golearn/12-gomod/02-00-greeting v0.0.0-00010101000000-000000000000
