package main

import "fmt"
import "rsc.io/quote"

// go mod init 模块名称
// go mod init命令会创建一个 go.mod 文件来跟踪代码的依赖项。到目前为止，该文件仅包含您的模块名称和您的代码支持的 Go 版本。
// 但是当您添加依赖项时，go.mod 文件将列出您的代码所依赖的版本。这使构建可重现，并使您可以直接控制要使用的模块版本。
// go mod tidy 下载依赖包

func main() {
	fmt.Println("hello, world")
	fmt.Println(quote.Go())
}
