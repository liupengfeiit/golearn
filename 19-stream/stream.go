package stream

type (
	Predicate func(v interface{}) bool
)
