package main

import "fmt"

const t = 1

const (
	a1 = 1
	a2
	a3
	a4
)
const s = 1

const (
	ok    = 200
	error = 500
)

// n2 n3 为100
const (
	b1 = 100
	b2
	b3
)

const (
	c1 = iota //0
	c2        //1
	c3        //2
	c4        //3
)

const (
	d1 = iota //0
	d2        //1
	_
	d4 //3
)
const (
	e1 = iota //0
	e2 = 100  //100
	e3 = iota //2
	e4        //3
)
const n5 = iota //0

const (
	_  = iota
	KB = 1 << (10 * iota)
	MB = 1 << (10 * iota)
	GB = 1 << (10 * iota)
	TB = 1 << (10 * iota)
	PB = 1 << (10 * iota)
)

const (
	a, b = iota + 1, iota + 2 //1,2
	c, d                      //2,3
	e, f                      //3,4
)

func main() {
	fmt.Println(t)
	fmt.Println(a1)
	fmt.Println(a2)
	fmt.Println(a3)
	fmt.Println(a4)

}
