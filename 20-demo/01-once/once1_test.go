package once1

import "testing"

func TestOnce(t *testing.T) {
	demo1()
	demo2()
}
