package once1

import (
	"fmt"
	"sync"
)

var (
	once  sync.Once
	once1 sync.Once
)

func demo1() {
	once.Do(func() {
		fmt.Println("demo1")
	})
}

func demo2() {
	once1.Do(func() {
		fmt.Println("demo2")
	})
}
