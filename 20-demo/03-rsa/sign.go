package main

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"strings"
)

func main() {
	str := "YoFghtBBTEHjizBnX++Bt0Fk9raXXO1mEnsA1Quc1PvkrpRljP6ZgF45ndMyPtm5IUc1kJgKK0VLGX5Jv8Odln2l5zJqFvIB1LNFyArizdl4OCbejBmsMggAMsuQjf/YJOWWkRISyW9xbzQehB3z0fIO4eKVD3elsg8Uhvwo+D40nh8uSfDgHOJ0PlbegygxW0J3S31s87kMBTtVuhTnyONTduqNp/+keDoIYH/pzbaUb/7iZ5F6YSieu3u7Im5qmfheUfYYAguQsbo16yZ8wzxE0M4UKFcYL7Erzvvj3a8yXC5OmPDpO8ssWPNYFJ0mEpYbAcgo1GSBX2S8Qhlz1Q=="
	this := &Sign{}
	_, msg := this.ParseSign(str)
	fmt.Println(msg)
}

type Sign struct{}

const (
	privateKey = `MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCFfYjE5LJA7Trqnxv+dTNrOU39FEOvOtesekxQHKn7/DeHra80DAtEBuCnxYCk3QH6p0LP2zVuiBye6p47gxZSE7am/jNh5MjYA7UEpNLS9JGgQfO8u8dLuvUNN/GG+Z8HS2v8n7b5bBfRYrWlNXdQkVdCJU93gn91PcrYsglDic1pGpHiOr5eeaHF+BZ5gWufCoS5vfPJgQzQkH/ZOzwse9Hf3dp93kN8PzKD1Mj0rO6yI2uzm+PtWwHWkiJcBIsejgzR2MlZHYJZiEhUcF067h/As1yZYKCcN4yKuRoe84YDP52QWXdHz+8m+STWAvwbHBigLtGZK3YdHR3eZdlbAgMBAAECggEAV860eXdJvaWOnKhefKnRUdWW9EMxJoGZPq4Ppp7EPamxSUE5Pw4xFuVDP3iN32Q1GhCNxTYXVoaX9Py1asYWl5S6D+ykFd+tMVJ27wAQYY1YgUcAsgP+21S2PqSu7i8iqWS2Oq1AaZ7tp+C7RVn/CvMfp7XtKNqhRpiH5rRtnYuBqhThIlk8SBdMn+0lzcUj4iU+pTkDXzr98SCcpVtFiPCA7kAXG6vNr3tk63XZ9cVdZVxBqe9h6unMHPJ62/NusvtaQIZmx5TgZgeKt+aP4ZlZ0Aeiffivtym0MXXag+s0OfaTV1iw2KRMfZ9hZ433pSsgsG81GR5nEZ8Ala0iKQKBgQDQeNek9hXAM2Bfa5kFcZisW9q6UFdJurbzuTHA3s2JBjZYLUPDjjLlcgDMZWvkU6+eY6ZhOuCePwHEFg2NKB0x/dYMGttv9cyxurvf2ej6dIw0bysiuJOr1X4YtidR25lvLvn6HqrFt9VlVjcfkPos0Jzw93CBfWQ0q9zWLCoXbQKBgQCj7H/JhyvM7Mb73k6SN1JscwOaoNgjaILXehBIZlYYXOkjsVlJif32gfTIntd1U7d2/0QON7YZxsTmIcYpgC/mbKpEC6ZonsFp6OF8MQ0JnUNtyJzoFBoIcHdWdrxolBSmcwHwQJPJEJSfQ/tr3Mn2Szj4KojSgLL2f8s8/DXO5wKBgDlDmGiVuC/NbQGI+sWGCJv/tztKak+0XCDilAoYqXikHjVmaaDD5LMl95Ytnc9gNXNaiA9ZLu4I/KEyNZK6nBppUWyNzA0bBZkkZAr/PoHdlc2pY73r2ZRv1m1PS8muep0R2FSeK+aXsqRdZFLlbfL1csa2EU9gdDALrWOfSgiZAoGAJ7RnF2c4QsCvSd6E5YGk40PQgamn8Jul5Y9ZVlADVo9aoRyZ5egYrZa4tcdTpMWMULRvxYnU/9VXZ+6SUwcLkyKK80RnIDwj2QVhaHVxFP0UkgjfiI8l5xlmrnjAFgyYG+ELfSgHI9JyTUloIq4t9QwvVlznGyW1M2Os1K66Uc0CgYBCu0b0Iws4djQCkOr+WKiAXiLJkzqyNBrnP3DPryVPf9YGyFpwMN77/f65gS27yn2kEOc4H3iYw3UQYx3NI4yNzuRXepXkT6XZfhmN7PmNIJ9KdWnQ9LIi0mu8YEwv4NFuFmc4qlGzDHJwGYMtCZXrLCfd/lTiaj6bXflj5KMs/g==`
)

func (this *Sign) GenerateSignature(prvKeyPem, indata string) (error, []byte) {
	block, _ := pem.Decode([]byte(prvKeyPem))
	if block == nil {
		return errors.New("failed to parse root certificate PEM"), nil
	}
	privKey, err := x509.ParsePKCS8PrivateKey(block.Bytes) //x509.ParseCertificate(block.Bytes)
	if err != nil {
		return err, nil
	}
	h := sha256.New()
	h.Write([]byte(indata))
	digest := h.Sum(nil)
	s, err := rsa.SignPKCS1v15(rand.Reader, privKey.(*rsa.PrivateKey), crypto.SHA256, digest)
	if err != nil {
		return err, nil
	}
	return nil, s
}

func (this *Sign) WrapString(s string, limit int) string {
	init := make([]byte, 0, len(s))
	if len(s) == 0 {
		return ""
	}
	buf := bytes.NewBuffer(init)
	n := 0
	for _, char := range s {
		n++
		buf.WriteRune(char)
		if n%limit == 0 {
			buf.WriteRune('\n')
		}
	}
	return buf.String()
}

func (this *Sign) DecodeBase64(in string) []byte {
	out := make([]byte, base64.StdEncoding.DecodedLen(len(in)))
	n, err := base64.StdEncoding.Decode(out, []byte(in))
	if err != nil {
		return nil
	}
	return out[0:n]
}

func (this *Sign) Encode(signature []byte) string {
	sign := base64.StdEncoding.EncodeToString(signature)
	str := string(sign)
	str = strings.Replace(str, "+", "-", -1)
	str = strings.Replace(str, "/", "_", -1)
	str = strings.Replace(str, "=", ".", -1)
	return str
}

func (this *Sign) ParseSignature(prvKeyPem, indata string) (error, string) {
	block, _ := pem.Decode([]byte(prvKeyPem))
	if block == nil {
		return errors.New("failed to parse root certificate PEM"), ""
	}
	privKey, err := x509.ParsePKCS8PrivateKey(block.Bytes) //x509.ParseCertificate(block.Bytes)
	if err != nil {
		return err, ""
	}
	//h := sha256.New()
	//h.Write([]byte(indata))
	//digest := h.Sum(nil)
	s, err := rsa.DecryptPKCS1v15(rand.Reader, privKey.(*rsa.PrivateKey), []byte(indata))
	// s, err := rsa.SignPKCS1v15(rand.Reader, privKey.(*rsa.PrivateKey), crypto.SHA256, digest)
	if err != nil {
		return err, ""
	}
	return nil, string(s)
}

// 用于获得签名
func (this *Sign) GetSign(content string) (error, string) {
	pkey := fmt.Sprintf("-----BEGIN RSA PRIVATE KEY-----\n%s\n-----END RSA PRIVATE KEY-----", this.WrapString(privateKey, 64))
	err, signature := this.GenerateSignature(pkey, content)
	return err, this.Encode(signature)
}

// 用于解析签名
func (this *Sign) ParseSign(content string) (error, string) {
	pkey := fmt.Sprintf("-----BEGIN RSA PRIVATE KEY-----\n%s\n-----END RSA PRIVATE KEY-----", this.WrapString(privateKey, 64))
	return this.ParseSignature(pkey, content)
}
