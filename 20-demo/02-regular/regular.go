package main

import (
	"fmt"
	"regexp"
)

func main() {
	pattern := "(([\u4E00-\u9FA5A-Za-z0-9_]*)[:：]\\s*)?卡号[为]?[:：]\\s*(\\w*)\\s*(密码|卡密)[为]?[:：]\\s*(\\w*)"
	str := "素质素养测试: 卡号： ceshidan 密码： 1haokeceshi 东山老师课程专属: 卡号: ceshi 密码： 0802ceshi1"

	b, err := regexp.Match(pattern, []byte(str))
	fmt.Printf("%+v,%+v\n", b, err)

	reg, _ := regexp.Compile(pattern)
	result := reg.FindStringSubmatch(str)
	fmt.Printf("%+v\n", result)
	fmt.Println(len(result))
	str1 := "卡号： ceshidan 密码： 1haokeceshi"
	result1 := reg.FindStringSubmatch(str1)
	fmt.Printf("%+v\n", result1)
	fmt.Println(len(result1))

	result2 := reg.FindAllStringSubmatch(str, -1)
	fmt.Printf("%+v\n", result2)

	fmt.Println("------------------------")
	str2 := "卡号为：GA0025XD8DS9004C2Z 卡密为：823020kcyj3shykm"
	result3 := reg.FindStringSubmatch(str2)
	fmt.Printf("%+v\n", result3)
	fmt.Println(len(result3))
	groups := reg.FindAllStringSubmatch(str2, -1)
	for _, v := range groups {
		fmt.Printf(v[3])
		fmt.Printf(v[4])
		fmt.Printf(v[5])

	}
}
