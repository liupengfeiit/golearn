package main

import "fmt"

// AnimalIF 本质上是一个指针
type AnimalIF interface {
	Sleep()
	GetColor() string // 获取动物的颜色
	GetType() string  // 获取动物的种类
}

type Cat struct {
	color string
}

type Dog struct {
	color string
}

func (c *Cat) Sleep() {
	fmt.Println("cat is sleep...")
}

func (c *Cat) GetColor() string {
	return c.color
}

func (c *Cat) GetType() string {
	return "Cat"
}

func (d *Dog) Sleep() {
	fmt.Println("dog is sleep...")
}

func (d *Dog) GetColor() string {
	return d.color
}

func (d *Dog) GetType() string {
	return "Dog"
}

func printAnimal(animal AnimalIF) {
	fmt.Println("-----------------")
	animal.Sleep()
	fmt.Println("color = ", animal.GetColor())
	fmt.Println("type = ", animal.GetType())
}

// 编译时检查
var _ AnimalIF = new(Cat)

// 运行时检查
var _ AnimalIF = (*Cat)(nil)

func main() {
	var animal AnimalIF // 接口的数据类型，父类指针
	animal = &Cat{"Black"}
	animal.Sleep() // 调用Cat的Sleep方法,多态现象

	animal = &Dog{"Yellow"}
	animal.Sleep() // 调用Dog的Sleep方法, 多态现象

	cat := Cat{"Black"}
	dog := Dog{"Yellow"}

	printAnimal(&cat)
	printAnimal(&dog)
}
