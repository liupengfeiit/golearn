package main

import "fmt"

// extend 模拟继承

// Human 人类
type Human struct {
	name string
	sex  string
}

func (h *Human) Eat() {
	fmt.Println("Human eat ...", h)
}

func (h *Human) Walk() {
	fmt.Println("Human walk ...", h)
}

type SuperMan struct {
	// 结构体包含另外一个结构体，不给结构体起名称,则相当于继承
	// SuperMan包含了Human的所有函数
	Human
	level int
}

// Eat 重写父类的方法Eat
func (s *SuperMan) Eat() {
	fmt.Println("Super man eat ....", s)
}

// Fly 子类新的方法
func (s *SuperMan) Fly() {
	fmt.Println("Super man fly ....", s)
}

func (s *SuperMan) Print() {
	fmt.Println("name = ", s.name)
	fmt.Println("sex = ", s.sex)
	fmt.Println("level = ", s.level)
}

func main() {
	h := Human{"zhangsan", "男"}
	h.Eat()
	h.Walk()
	// s := SuperMan{h, 99}
	var s SuperMan
	s.name = "lisi"
	s.sex = "女"
	s.level = 99
	s.Walk() // 父类方法
	s.Eat()  // 子类方法
	s.Fly()  // 子类方法
	s.Print()
	fmt.Println("-------------------")
	// 定义父结构体实例
	f := Father{"fa"}
	f.talk() // 父结构体的talk函数
	// 定义子结构体实例 方式1
	son := Son{Father{"son"}, 11}
	// 定义子结构体实例 方式2
	var s1 Son
	// 定义子结构体实例 方式3
	s2 := Son{}
	// 赋值父结构体属性
	s1.name = "son"
	// 赋值子结构体属性
	s1.age = 11
	s1.talk()
	s2.talk()
	son.talk()
}


// 父结构体
type Father struct {
	name string
}
// 子结构体
type Son struct{
	Father
	age int
}
// 父结构体的方法
func (f *Father) talk() {
	fmt.Println("father talk ...")
}
// 子结构体重写父结构体的方法
func (s *Son) talk() {
	fmt.Println("son talk ...")
}

