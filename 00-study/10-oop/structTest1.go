package main

import "fmt"

// 结构体, 名称首字母大写， 表示其他包可以访问
// 结构体是值传递
type Hero struct {
	// 属性首字母大写，表示属性可以对外访问，否则只能在包内部访问
	Name  string
	Age   int
	level int
}

// 给结构体绑定函数
func (h *Hero) show() {
	fmt.Println("Name = ", h.Name)
	fmt.Println("Age = ", h.Age)
	fmt.Println("level = ", h.level)
}

func (h *Hero) GetName() string {
	return h.Name
}
func (h *Hero) SetName(name string) {
	h.Name = name
}
// 值传递
func changeName(h Hero) {
	h.Name = "Init"
}
// 引用传递,即指针传递
func changeName1(h *Hero) {
	h.Name = "hh"
}

func main() {
	h := Hero{
		Name: "蜘蛛侠",
		Age: 188,
		level: 10,
	}
	h.show()
	fmt.Println(h.GetName())
	h.SetName("钢铁侠")
	fmt.Println(h.GetName())
	h.show()
	var h1 Hero
	fmt.Println(h1)
	changeName(h1)
	fmt.Println(h1)
	changeName1(&h1)
	fmt.Println(h1)

}