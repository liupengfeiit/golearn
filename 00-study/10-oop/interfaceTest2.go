package main

import "fmt"

// interface 是万能数据类型
func myFunc(arg interface{}) {
	fmt.Println("-------------------")
	fmt.Println("my func is called...")
	fmt.Println(arg)
	fmt.Printf("arg type is %T\n", arg)
	// 如何区分interface{} 是什么数据类型
	// 给interface{} 提供"类型断言"机制
	value, ok := arg.(string)
	if !ok {
		fmt.Println("arg is not string type")
	} else {
		fmt.Println("arg is string type, value = ", value)
		fmt.Printf("value type is %T\n", value)
	}
}

type Book struct {
	name string
}
func main() {
	b := Book{"golang"}
	myFunc(b)
	myFunc(100)
	myFunc(3.14)
	myFunc("hello")

	var c bool
	c = true
	myFunc(c)
}