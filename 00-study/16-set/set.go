package main

import (
	"fmt"
	mapset "github.com/deckarep/golang-set/v2"
	"unsafe"
)

// set 实现 map[T]struct{}
func main() {
	s1 := struct{}{}
	s2 := struct{}{}
	fmt.Println(s1 == s2)
	fmt.Println(&s1)
	fmt.Println(&s2)
	// 空的结构体 内存占用为0
	fmt.Printf("%p, %p\n", &s1, &s2)
	fmt.Println(unsafe.Sizeof(struct{}{}))

	// Create a string-based set of required classes.
	required := mapset.NewSet[string]()
	required.Add("cooking")
	required.Add("english")
	required.Add("math")
	required.Add("biology")
	required.Add("biology")
	required.Add("biology")
	required.Add("biology")
	fmt.Printf("%+v\n", required)

	// Create a string-based set of science classes.
	sciences := mapset.NewSet[string]()
	sciences.Add("biology")
	sciences.Add("chemistry")
	fmt.Printf("%+v\n", sciences)

	// Create a string-based set of electives.
	electives := mapset.NewSet[string]()
	electives.Add("welding")
	electives.Add("music")
	electives.Add("automotive")
	fmt.Printf("%+v\n", electives)

	// Create a string-based set of bonus programming classes.
	bonus := mapset.NewSet[string]()
	bonus.Add("beginner go")
	bonus.Add("python for dummies")
	fmt.Printf("%+v\n", bonus)

}
