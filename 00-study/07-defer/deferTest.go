package main

import "fmt"

func main() {
	defer fmt.Println("defer 1")
	defer fmt.Println("defer 2")

	fmt.Println("main 1")
	fmt.Println("main 2")
	fmt.Println(testDefer())
	fmt.Println("---------------")
	returnAndDefer()
}

func testDefer() (int) {
	defer fmt.Println("test defer 1")
	defer fmt.Println("test defer 2")
	return 1
}

func deferFunc() int {
	fmt.Println("defer func called ....")
	return 0
}
func returnFunc() int {
	fmt.Println("return func called ....")
	return 0
}
// 测试执行顺序
func returnAndDefer() int {
	defer deferFunc()
	return returnFunc()
}