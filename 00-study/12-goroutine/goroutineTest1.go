package main

import (
	"fmt"
	"runtime"
	"time"
)

// 子goroutine
func newTask() {
	defer fmt.Println("new Goroutine defer")
	i := 0
	for {
		i ++
		fmt.Printf("new Goroutine: i = %d\n", i)
		time.Sleep(1 * time.Second)
		if i > 10 {
			// 终止当前goroutine, 之后仅 defer代码会执行
			runtime.Goexit()
			fmt.Println("new Goroutine for end")
		}
	}
	fmt.Println("new Goroutine end")
}
// 主goroutine
func main() {
	// 创建一个go 协程 去执行newTask()流程
	// go newTask()

	fmt.Println("main goroutine exit")
	go func() {
		defer fmt.Println("A.defer")
		func() {
			defer fmt.Println("B.defer")
			// 终止当前goroutine, 之后仅 defer代码会执行
			runtime.Goexit()
			fmt.Println("B")
		}()
		fmt.Println("A")
	}()

	go func(a, b int) bool {
		fmt.Println("a = ", a, "b = ", b)
		return true
	}(10, 14)

	i := 0
	for {
		i ++
		fmt.Printf("main Goroutine: i = %d\n", i)
		time.Sleep(1 * time.Second)
	}
}