package main

import "fmt"

// 测试指针

func main() {
	a, b := 10, 20
	swap(a, b)
	fmt.Println("a = ", a, "b = ", b)
	// & 取地址
	swapPoint(&a, &b)
	fmt.Println("a = ", a, "b = ", b)

	p := &a
	fmt.Println(&a)
	fmt.Println(p)

	var pp **int // 二级指针
	pp = &p
	fmt.Println(&p)
	fmt.Println(pp)
}
// 值传递,交换无效
func swap(a, b int) {
	temp := a
	a = b
	b = temp
}
// 传递指针  * 指针
func swapPoint(a, b *int) {
	// *a 代表指针的值  a代表指针地址
	fmt.Println("point a = ", a , " ", *a)
	fmt.Println("point b = ", b , " ", *b)

	temp := *a
	*a = *b
	*b = temp
}