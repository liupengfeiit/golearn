package main

import "fmt"

type Reader interface {
	ReadBook()
}

type Writer interface {
	WriteBook()
}

type Book struct {
}

func (b *Book) ReadBook() {
	fmt.Println("Read a book")
}

func (b *Book) WriteBook() {
	fmt.Println("Write a book")
}

func main() {
	// b 是Book实例的一个指针
	// b: pair<type: Book, value: book{}地址>
	b := &Book{}
	// r: pair<type: , value: >
	var r Reader
	// r: pair<type: Book, value: book{}地址>
	r = b
	r.ReadBook()
	var w Writer
	// r: pair<type:Book, value: book{}地址>
	w = r.(Writer) // 此处断言为什么回成功? 因为w r 具体的type是一致的
	w.WriteBook()
	w = b
	w.WriteBook()
}
