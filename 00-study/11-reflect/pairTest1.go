package main

import "fmt"

func main() {

	var a string
	// pair <static type: string, value: "hello">
	a = "hello"
	fmt.Println(a)

	var allType interface{}
	allType = a
	str, _ := allType.(string)
	fmt.Println(str)
}
