package main

import (
	"fmt"
	"reflect"
)

type User struct {
	Id   int
	Name string
	Age  int
}

func (u *User) Call() {
	fmt.Println("user is call")
	fmt.Printf("%v\n", u)
}

func main() {
	user := User{1, "zhangsan", 19}
	// DoFiledAndMethod(user)
	DoFiledAndMethod1(&user)
}

// DoFiledAndMethod 反射获取参数的属性和方法
func DoFiledAndMethod(i interface{}) {
	// 获取i的 type
	inputType := reflect.TypeOf(i)
	fmt.Println("i type is :", inputType.Name())
	// 获取i的value
	inputValue := reflect.ValueOf(i)

	// 通过type获取里面的字段
	// 1. 获取interface的reflect.Type, 通过Type得到NumFiled 字段的数量,进行遍历
	// 2. 得到每个filed，数据类型
	// 3. 通过filed有一个Interface 方法得到 对应的value
	for i := 0; i < inputType.NumField(); i++ {
		field := inputType.Field(i)
		value := inputValue.Field(i).Interface()
		fmt.Printf("%s: %v = %v\n", field.Name, field.Type, value)
	}

	// 获取不到指针指向的方法
	// 通过type获取里面的方法进行调用
	for i := 0; i < inputType.NumMethod(); i++ {
		m := inputType.Method(i)
		fmt.Printf("%s: %v\n", m.Name, m.Type)
	}

}

// DoFiledAndMethod 反射获取参数的属性和方法
func DoFiledAndMethod1(i interface{}) {
	// 获取i的 type
	inputType := reflect.TypeOf(i)
	inputType = inputType.Elem()
	fmt.Println("i type is :", inputType.Name())
	// 获取i的value
	inputValue := reflect.ValueOf(i).Elem()

	// 通过type获取里面的字段
	// 1. 获取interface的reflect.Type, 通过Type得到NumFiled 字段的数量,进行遍历
	// 2. 得到每个filed，数据类型
	// 3. 通过filed有一个Interface 方法得到 对应的value
	for i := 0; i < inputType.NumField(); i++ {
		field := inputType.Field(i)
		value := inputValue.Field(i).Interface()
		fmt.Printf("%s: %v = %v\n", field.Name, field.Type, value)
	}

	// 获取不到指针指向的方法
	// 通过type获取里面的方法进行调用
	for i := 0; i < inputType.NumMethod(); i++ {
		m := inputType.Method(i)
		fmt.Printf("%s: %v\n", m.Name, m.Type)
	}

}
