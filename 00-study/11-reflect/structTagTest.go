package main

import (
	"fmt"
	"reflect"
)

// 结构体标签
type resume struct {
	Name string `info:"name" doc:"名字"`
	Sex  string `info:"sex"`
}

func findTag(str interface{}) {
	e := reflect.TypeOf(str)
	fmt.Println(e)
	t := e.Elem()
	for i := 0; i < t.NumField(); i++ {
		taginfo := t.Field(i).Tag.Get("info")
		tagdoc := t.Field(i).Tag.Get("doc")
		fmt.Println("info:", taginfo, "doc:", tagdoc)
	}
}

func main() {
	var re resume
	findTag(&re)
	rr := resume{"张三", "男"}
	findTag(&rr)
}
