package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
)

/*
* ./demo  --cpuprofile=cpu.prof --memprofile=mem.prof

go tool pprof cpu.prof

https://go.dev/blog/pprof
https://blog.csdn.net/zhanglu_1024/article/details/123867858
*/
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile `file`")

var memprofile = flag.String("memprofile", "", "write memory profile to `file`")

func main() {

	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		if err := pprof.StartCPUProfile(f); err != nil { //监控cpu
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	for i := 0; i < 100; i++ {
		fmt.Println(i)
	}

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		runtime.GC()                                      // GC，获取最新的数据信息
		if err := pprof.WriteHeapProfile(f); err != nil { // 写入内存信息
			log.Fatal("could not write memory profile: ", err)
		}
		f.Close()
	}

}
