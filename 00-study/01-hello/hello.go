package main // 程序的包名，如果需要main函数的话，必须使用main包才可以作为程序的入口

/** 导入多个pkg的两种方式 **/
/***
import "fmt"
import "time"
*/
import (
	"fmt"
	"time"
)

// main函数，程序主入口
func main() { // 函数 的  { 一定是和函数名在同一行的，否则变异错误
	// golang中的表达式，加";" 和不加都可以，建议是不加
	fmt.Println("hello Go!")
	//time.Sleep(1 * time.Second)
	// 2006-01-02
	s := "2022-11-28 14:59:00"
	t, _ := time.ParseInLocation("2006-01-02 15:04:05", s, time.Local)
	fmt.Println(t.Unix())
	fmt.Println(time.Now().Unix())
}
