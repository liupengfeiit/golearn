package main

import (
	"gitee.com/liupengfei_it/golearn/00-study/05-init/lib1"
	myLib2 "gitee.com/liupengfei_it/golearn/00-study/05-init/lib2"
)

func main() {
	lib1.Lib1Test()
	myLib2.Lib2Test()
}