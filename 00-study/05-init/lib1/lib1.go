package lib1

import (
	"fmt"
	"gitee.com/liupengfei_it/golearn/00-study/05-init/lib2"
)

func Lib1Test() {
	fmt.Println("lib1Test....")
	lib2.Lib2Test()
}

func init() {
	fmt.Println("lib1 init ....")
}