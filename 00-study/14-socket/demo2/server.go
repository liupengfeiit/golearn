package main

import (
	"fmt"
	"net"
	"sync"
)

type Server struct {
	Ip string
	Port int
	// 在线用户列表， key: string  value: user实例
	OnlineMap map[string]*User
	// 读写锁
	mapLock sync.RWMutex
	// 消息广播的channel
	Message chan string
}


func NewServer(ip string, port int) *Server {
	/*server := &Server{
		Ip: ip,
		Port: port,
	}
	return server;*/
	return &Server{ip, port, make(map[string]*User), sync.RWMutex{}, make(chan string)}
}


func (server *Server) Start() {
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", server.Ip, server.Port))
	if err != nil {
		fmt.Println("server listen err:", err)
		return
	}
	// server 关闭时,关闭端口监听
	defer listener.Close()
	// 监控服务器端的通道
	go server.listenMessage()
	fmt.Println("server start success...")
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("server accept err:", err)
			continue
		}
		go server.Handler(conn)
	}
}

// Handler 连接建立成功后处理流程
func (server *Server) Handler(conn net.Conn) {
	fmt.Println("连接建立成功...", "remote addr:", conn.RemoteAddr(), " local addr: ",conn.LocalAddr())
	// 创建用户实例
	user := NewUser(conn)
	// 将用户放到map中, 因为是并发操作,需要开启锁
	server.mapLock.Lock()
	server.OnlineMap[user.Name] = user
	server.mapLock.Unlock()
	// 广播当前用户上线消息
	server.broadCast(user, "已上线")


	// 阻塞当前用户handler
	select {}
}

// 广播用户上线消息
func (server *Server) broadCast(user *User, msg string) {
	sendMsg := "[" + user.Addr + "]" + user.Name + ":" + msg
	// 消息写入广播通道
	server.Message <- sendMsg
}

// 监听server的channel,用于广播给所有在线用户
func (server *Server) listenMessage() {
	for {
		msg := <-server.Message
		// 将msg消息发送给全部的在线User, 由于要访问map, map可能被并发修改,所以先加锁
		server.mapLock.Lock()
		for _, user := range server.OnlineMap {
			user.C <- msg
		}
		server.mapLock.Unlock()
	}
}

