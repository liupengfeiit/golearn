package main

import "net"

type User struct {
	Name string
	Addr string
	// 用户持有的通道
	C chan string
	// 用户的连接
	conn net.Conn
}

func NewUser(conn net.Conn) *User {
	userAddr := conn.RemoteAddr().String()
	user := &User{
		Name: userAddr,
		Addr: userAddr,
		C: make(chan string),
		conn: conn,
	}
	// 启动监听当前user的channel
	go user.ListenMessage()
	return user
}

// ListenMessage 监听当前user的channel，一旦channel中有消息，就直接发送给客户端
func (user *User) ListenMessage() {
	for {
		// 从通道中写入数据
		msg := <-user.C
		user.conn.Write([]byte(msg + "\n"))
	}
}