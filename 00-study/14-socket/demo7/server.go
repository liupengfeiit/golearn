package main

import (
	"fmt"
	"net"
	"sync"
	"time"
)

type Server struct {
	Ip string
	Port int
	OnlineMap map[string]*User
	mapLock sync.RWMutex // 继承
	Message chan string
}


func NewServer(ip string, port int) *Server {
	return &Server{ip, port, make(map[string]*User), sync.RWMutex{}, make(chan string)}
}


func (server *Server) Start() {
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", server.Ip, server.Port))
	if err != nil {
		fmt.Println("listen open err:", err)
		return
	}
	defer listener.Close()
	fmt.Printf("server start success[%s:%d]\n", server.Ip, server.Port)
	// 开启Server message监听
	go server.listenMessage()
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("建立连接失败:", err)
			continue
		}
		// 启动goroutine处理conn
		go server.handler(conn)
	}
}

// 监听Server message 推送给所有在线用户
func (server *Server) listenMessage() {
	for {
		msg := <-server.Message
		server.mapLock.Lock()
		for _, user := range server.OnlineMap {
			user.C <- msg
		}
		server.mapLock.Unlock()
	}
}

func (server *Server) handler(conn net.Conn) {
	fmt.Println("连接建立成功... remote addr:", conn.RemoteAddr(), " local addr:", conn.LocalAddr())
	user := NewUser(conn, server)
	user.OnLine()
	go user.ReadMsg()
	// 当前 goroutine 阻塞， 否则执行完成就关闭了 子goroutine
	for {
		select {
			case <-user.isAlive:
				//当前用户是活跃的，应该重置定时器
				//不做任何事情，为了激活select，更新下面的定时器
			case <-time.After(30 * time.Minute):
				//已经超时
				//将当前的User强制的关闭
				user.SendMsg("你被踢了\n")
				user.OffLine()
				// 销毁user用的资源
				user.Close()
				// 结束当前处理程序
				return
		}
	}
}

func (server *Server) BroadCast(user *User, msg string) {
	sendMsg := "[" + user.Addr + "]" + user.Name + ":" + msg
	server.Message <- sendMsg
}