package main

// 构建一个基础Server
// 广播上线功能
// 用户消息广播机制
// 消息格式who 查询所有在线人员列表
// 用户修改名称功能 加上对“rename|张三”指 令的处理理，返回在线⽤用户信息
func main() {
	server := NewServer("127.0.0.1", 8888)
	server.Start()
}