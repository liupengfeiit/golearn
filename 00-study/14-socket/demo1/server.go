package main

import (
	"fmt"
	"net"
)

type Server struct {
	Ip string
	Port int
}

// NewServer 返回结构体的实例指针
func NewServer(ip string, port int) *Server {
	server := Server{
		Ip: ip,
		Port: port,
	}
	return &server
}

func (server *Server) Start() {
	// fmt.Sprintf 格式化字符串
	// socket listen 开启网络监听，使用tcp通信，返回端口的监听器
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", server.Ip, server.Port))
	if err != nil {
		fmt.Println("net.listener err:", err)
		return
	}
	fmt.Printf("server start , port:%d\n", server.Port)
	// 方法结束后关闭监听, 释放端口资源
	defer listener.Close()
	for {
		// 接收网络请求
		conn, err := listener.Accept();
		if err != nil {
			fmt.Println("listener accept err:", err)
			continue
		}
		go server.Handler(conn)
	}
}

// Handler 连接处理 Net.Conn 是一个接口,接口本身是一个指针
func (server *Server) Handler(conn net.Conn) {
	fmt.Println("连接建立成功")
}