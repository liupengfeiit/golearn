package main

import (
	"fmt"
	"io"
	"net"
)

type User struct {
	Name string
	Addr string
	C chan string
	conn net.Conn
	server *Server
}


func NewUser(conn net.Conn, server *Server) *User {
	addr := conn.RemoteAddr().String()
	user := &User{addr, addr, make(chan string), conn, server}
	go user.listenChan()
	return user
}


func (user *User) listenChan() {
	for {
		msg := <-user.C
		user.conn.Write([]byte(msg + "\n"))
	}
}

func (user *User) OnLine()  {
	user.server.mapLock.Lock()
	user.server.OnlineMap[user.Name] = user
	user.server.mapLock.Unlock()
	user.broadCast("已上线")
}

func (user *User) OffLine() {
	user.server.mapLock.Lock()
	delete(user.server.OnlineMap, user.Name)
	user.server.mapLock.Unlock()
	user.broadCast("已下线")
}

func (user *User) broadCast(msg string) {
	user.server.BroadCast(user, msg)
}

func (user *User) ReadMsg() {
	buf := make([]byte, 4096)
	for {
		n, err := user.conn.Read(buf)
		if n == 0 {
			// n = 0 缓冲去关闭, 用户下线
			user.OffLine()
			return
		}
		if err != nil && err != io.EOF {
			fmt.Println("Conn Read err:", err)
			return
		}
		//提取用户的消息(去除'\n')
		msg := string(buf[:n-1])
		user.dispatchHandler(msg)
	}
}

func (user *User) dispatchHandler(msg string) {
	fmt.Println("user:", user.Name, " msg: ", msg)
	if msg == "who" {
		// 查询当前都有哪些用户在线
		user.server.mapLock.Lock()
		for _, u := range user.server.OnlineMap {
			onlineMsg := "[" + u.Addr + "]" + u.Name + ":" + "在线...\n"
			user.SendMsg(onlineMsg)
		}
		user.server.mapLock.Unlock()
	} else {
		user.broadCast(msg)
	}
}

func (user *User) SendMsg(msg string) {
	user.conn.Write([]byte(msg))
}