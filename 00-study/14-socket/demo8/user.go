package main

import (
	"fmt"
	"io"
	"net"
	"strings"
)

type User struct {
	Name    string
	Addr    string
	C       chan string
	conn    net.Conn
	server  *Server
	isAlive chan bool
}

func NewUser(conn net.Conn, server *Server) *User {
	addr := conn.RemoteAddr().String()
	user := &User{addr, addr, make(chan string), conn, server, make(chan bool)}
	go user.listenChan()
	return user
}

func (user *User) listenChan() {
	for {
		msg := <-user.C
		user.conn.Write([]byte(msg + "\n"))
	}
}

func (user *User) OnLine() {
	user.server.mapLock.Lock()
	user.server.OnlineMap[user.Name] = user
	user.server.mapLock.Unlock()
	user.broadCast("已上线")
}

func (user *User) OffLine() {
	user.server.mapLock.Lock()
	delete(user.server.OnlineMap, user.Name)
	user.server.mapLock.Unlock()
	user.broadCast("已下线")
}

func (user *User) broadCast(msg string) {
	user.server.BroadCast(user, msg)
}

func (user *User) ReadMsg() {
	defer close(user.isAlive)
	buf := make([]byte, 4096)
	for {
		n, err := user.conn.Read(buf)
		if n == 0 {
			// n = 0 缓冲去关闭, 用户下线
			user.OffLine()
			return
		}
		if err != nil && err != io.EOF {
			fmt.Println("Conn Read err:", err)
			return
		}
		// 用户的任意消息，代表当前用户是一个活跃的

		user.isAlive <- true
		//提取用户的消息(去除'\n')
		msg := string(buf[:n-1])
		user.dispatchHandler(msg)
	}
}

func (user *User) dispatchHandler(msg string) {
	fmt.Println("user:", user.Name, " msg: ", msg)
	if msg == "who" {
		// 查询当前都有哪些用户在线
		user.server.mapLock.Lock()
		for _, u := range user.server.OnlineMap {
			onlineMsg := "[" + u.Addr + "]" + u.Name + ":" + "在线...\n"
			user.SendMsg(onlineMsg)
		}
		user.server.mapLock.Unlock()
	} else if len(msg) > 7 && msg[:7] == "rename|" {
		//消息格式: rename|张三
		newName := strings.Split(msg, "|")[1]
		// map中 newName是否存在 _ 值, ok 是否存在
		_, ok := user.server.OnlineMap[newName]
		if ok {
			user.SendMsg("当前用户名已被使用\n")
		} else {
			user.server.mapLock.Lock()
			delete(user.server.OnlineMap, user.Name)
			user.server.OnlineMap[newName] = user
			user.server.mapLock.Unlock()
			user.Name = newName
			user.SendMsg("您已经更新用户名:" + user.Name + "\n")
		}
	} else if len(msg) > 4 && msg[:3] == "to|" {
		// 消息格式:  to|张三|消息内容
		//1 获取对方的用户名
		content := strings.Split(msg, "|")
		toName := content[1]
		if toName == "" {
			user.SendMsg("消息格式不正确，请使用 \"to|张三|你好啊\"格式。\n")
			return
		}
		//2 根据用户名 得到对方User对象
		toUser, ok := user.server.OnlineMap[toName]
		if !ok {
			user.SendMsg("该用户名不不存在\n")
			return
		}
		//3 获取消息内容，通过对方的User对象将消息内容发送过去
		toMsg := content[2]
		if toMsg == "" {
			user.SendMsg("无消息内容，请重发\n")
			return
		}
		toUser.SendMsg(user.Name + "对您说:" + toMsg + "\n")
	} else {
		user.broadCast(msg)
	}
}

func (user *User) SendMsg(msg string) {
	user.conn.Write([]byte(msg))
}

// 释放资源
func (user *User) Close() {
	user.SendMsg("连接已关闭，资源已释放\n")
	close(user.C)
	// close(user.isAlive)
	user.conn.Close()
}
