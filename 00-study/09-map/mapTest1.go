package main

import "fmt"

func main() {
	cityMap := make(map[string]string)
	cityMap["China"] = "BeiJing"
	cityMap["Japan"] = "Tokyo"
	cityMap["USA"] = "h"
	printMap(cityMap)       // 遍历
	delete(cityMap, "USA")  // 删除
	cityMap["China"] = "BJ" // 修改
	change(cityMap)         // 引用传递修改
	printMap(cityMap)       // 验证是否为引用传递
}

func printMap(m map[string]string) {
	for key, value := range m {
		fmt.Println("key = ", key, " value = ", value)
	}
	fmt.Println("-----------------")
}

func change(m map[string]string) {
	m["England"] = "London"
}
