package main

import "fmt"

func main() {
	// 1. 第一种声明map的方式
	var map1 map[string]string // map1 key = string, value = string
	if map1 == nil {
		fmt.Println("map1 is nil, 无空间占用")
	}
	// 使用map前，需要先用 make 给map分配数据空间
	map1 = make(map[string]string, 10)
	map1["one"] = "c"
	map1["two"] = "c++"
	map1["three"] = "java"
	fmt.Println(map1)
	// 遍历map
	for key, value := range map1 {
		fmt.Println("key = ", key, " value = ", value)
	}

	// 2. 第二种声明map的方式
	map2 := make(map[int]string) // map2 key = int ,value = string
	map2[1] = "java"
	map2[2] = "c"
	map2[3] = "go"
	fmt.Println(map2)

	// 3. 第三种声明方式
	map3 := map[string]string{
		"one": "go",
		"two": "c",
		"three": "c++",
	}
	fmt.Println(map3)
}
