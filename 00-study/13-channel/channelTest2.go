package main

import (
	"fmt"
	"time"
)
// 测试带缓冲区的channel
func main() {
	c := make(chan int, 3) // 带有缓冲区的channel
	fmt.Println("len(c) = ", len(c), ", cap(c)", cap(c))
	go func() {
		defer fmt.Println("子goroutine 结束")
		for i := 0; i < 4; i++ {
			c <- i // 将i写入通道中
			fmt.Println("子goroutine正在运行,发送的元素=", i, " len(c) = ", len(c), ", cap(c) =", cap(c))
		}
	}()
	time.Sleep(1 * time.Second)
	for i := 0; i < 4; i++ {
		num := <-c
		fmt.Println("num = ", num)
	}
	time.Sleep(1 * time.Second)
	fmt.Println("main结束")
}
