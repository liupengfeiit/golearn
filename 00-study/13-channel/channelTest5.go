package main

import "fmt"

// 斐波那契数列
func fibonacii(c, quit chan int) {
	// 1, 1, 2, 3,5
	x, y := 1, 1
	for {
		select {
		case c <- x:
			// 如果c可写，则该case会执行
			temp := y
			y = x + y
			x = temp
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func main() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	fibonacii(c, quit)
}