package main

import (
	"fmt"
	"time"
)

func main() {
	// 定一个存放int类型的通道,通道容量为1
	c := make(chan int)
	c1 := make(chan bool)
	go func() {
		defer fmt.Println("goroutine 结束")
		fmt.Println("goroutine 正在运行...")
		c <- 666 // 将666 发送到通道c中
		time.Sleep(5 * time.Second)
		c1 <- true
	}()
	num := <-c // 从c中接受数据，并赋值给num，如果c通道中没有数据则阻塞等待
	fmt.Println("num =", num)
	<-c1 // 阻塞等待获取c1中的数据
	fmt.Println("main goroutine end")
}


