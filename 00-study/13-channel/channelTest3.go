package main

import "fmt"

func main() {
	c := make(chan int)
	go func() {
		defer close(c)
		for i := 0; i < 5; i++ {
			c <- i
		}
		// close 可以关闭一个channel
		// close(c)
	}()

	for {
		// ok = true 表示channel没有关闭，如果为false表示channel已经关闭
		if data, ok := <-c; ok {
			fmt.Println(data)
		} else {
			fmt.Println("channel is closed")
			break
		}
 	}
 	fmt.Println("Main Finished..")
}