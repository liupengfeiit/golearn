package main

import "log"

func main() {
	c := make(chan struct{})
	close(c)
	select {
	case <-c:
		log.Println("c exit")
	default:
		log.Println("c not exit")
	}
}
