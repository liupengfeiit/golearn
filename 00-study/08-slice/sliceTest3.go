package main

import "fmt"

func main() {
	// len = 3, cap = 3 ["1", "2", "3"]
	s := []string{"1", "2", "3"}

	// 取s第0位开始，向后取两位元素 [0, 2]
	s1 := s[0:2] // ["1", "2"]
	fmt.Println("s1 = ", s1)
	cs := s[0:1]
	fmt.Println(cs)
	s1[0] = "100"
	// 切片截取后，是和原切片共用一块内存
	fmt.Println("s1 = ", s1)
	fmt.Println("s = ", s)
	s2 := make([]string, len(s)) // s2 = ["", "", ""]

	copy(s2, s) // 将s中的值 拷贝到s2中, 拷贝并不会触发容量扩容
	fmt.Println(s2)

	tes1()
	tes2()
	tes3()
	tes4()
	tes5()
	tes6()
}

func tes1() {
	seq := []string{"a", "b", "c", "d", "e"}
	// 指定删除位置
	index := 2

	// 将删除点前后的元素连接起来
	seq = append(seq[:index], seq[index+1:]...)
	fmt.Println(seq) //[a b d e]
}

func tes2() {

	seq := []string{"a", "b", "c", "d", "e"}
	// 指定删除位置
	index := 3

	// 将删除点前后的元素连接起来
	seq = append(seq[:0], seq[index+1:]...)
	fmt.Println(seq) //[e]
}

func tes3() {
	fmt.Println("--------------tes3")
	ss := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	index := 2
	dss := append(ss[:index], ss[index+1:]...)
	fmt.Println(dss) //[a b d e]
}

func tes4() {
	fmt.Println("--------------tes4")
	ss := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	index := 2
	css := ss[0:index]
	fmt.Println(css)
	fmt.Println(ss)
	dss := append(ss[:index], ss[index+1:]...)
	fmt.Println(dss) //[a b d e]
}

func tes5() {
	fmt.Println("--------------tes5")
	ss := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	index := 2
	css := ss[0:index]
	fmt.Println(css)
	fmt.Println(ss)
	dss := append(ss[:0], ss[index+1:]...)
	fmt.Println(dss) //[a b d e]
	fmt.Println(ss)

}

func tes6() {
	fmt.Println("--------------tes6")
	ss := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	fmt.Println(ss)
	index := 2
	dss1 := append(ss[:0], ss[index:]...)
	fmt.Println(dss1)
	fmt.Println(ss)
}
