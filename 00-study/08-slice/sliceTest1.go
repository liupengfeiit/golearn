package main

import "fmt"

func main() {
	// 声明一个切片， 并且初始化，默认值是1，2，3， 容量是3
	slice1 := []int{1, 2, 3}
	fmt.Printf("slice vlaue = %v, type = %T\n", slice1, slice1)
	// 声明slice2 是一个切片，但是没有给slice2分配空间
	var slice2 []int
	// 给slice2 开辟3个空间，默认值是0
	slice2 = make([]int, 3)
	fmt.Printf("slice vlaue = %v, len = %d, type = %T\n", slice2, len(slice2), slice2)
	// 声明slice3 是一个切片，同时给slice分配空间，3个空间，初始化值0，通过:=推导出slice3 是一个切片
	slice3 := make([]int, 3)
	fmt.Printf("slice vlaue = %v, len = %d, type = %T\n", slice3, len(slice3), slice3)
	if slice3 == nil {
		// 仅声明没有进行初始化或者 make的切片，则为nil
		fmt.Println("slice3 is nil ")
	} else {
		fmt.Println("slice3 不为nil ")
	}
	var slice4 []int
	slice4 = append(slice4, 3)
	fmt.Printf("%+v\n", slice4)
}
