package main

import "fmt"

func main() {
	// 固定长度的数组
	var myArray1 [10]int
	fmt.Println(myArray1)
	for i := 0; i < len(myArray1); i++ {
		fmt.Print(myArray1[i], " ")
	}
	fmt.Println("")
	fmt.Println("----2-----")
	// 固定长度的数组,并初始化几个元素
	myArray2 := [10]int{1,2,3,4}
	fmt.Println(myArray2)
	for index, value := range myArray2 {
		fmt.Println("index = ", index, " value = ", value)
	}
	fmt.Println("----3-----")
	myArray3 := [4]int{11,22,33,44}
	fmt.Println(myArray3)
	fmt.Println("----4-----")
	fmt.Printf("myArray1 types = %T\n", myArray1)
	fmt.Printf("myArray2 types = %T\n", myArray2)
	fmt.Printf("myArray3 types = %T\n", myArray3)
	fmt.Println("----5-----")
	setValue(myArray3)
	fmt.Println(myArray3)
	setPointValue(&myArray3)
	fmt.Println(myArray3)
}
// 值传递, 更改元素无效
func setValue(arr [4]int) {
	arr[1] = 100
}
// 形参设置了数组的容量， 调用时必须为这么大的容量的参数
func setPointValue(arr *[4]int) {
	(*arr)[1] = 100
}