package main

import "fmt"

func main() {
	// 开辟cap容量为 5，初始容量为3 的切片
	var nums = make([]int, 3, 5)
	fmt.Printf("len = %d, cpa = %d, slice = %v\n", len(nums), cap(nums), nums)
	// 向nums切片追加一个元素1， nums  len = 4 , [0, 0, 0, 1], cap = 5
	nums = append(nums, 1)
	fmt.Printf("len = %d, cpa = %d, slice = %v\n", len(nums), cap(nums), nums)
	// 向nums切片追加一个元素2， nums  len = 5 , [0, 0, 0, 1, 2], cap = 5
	nums = append(nums, 2)
	fmt.Printf("len = %d, cpa = %d, slice = %v\n", len(nums), cap(nums), nums)

	// 向一个容量cap已经满的slice追加元素, 容量翻倍
	nums = append(nums, 3)
	fmt.Printf("len = %d, cpa = %d, slice = %v\n", len(nums), cap(nums), nums)
	fmt.Println("---------------------")
	// 初始容量为3， cap容量为3
	var nums1 = make([]int, 3)
	fmt.Printf("len = %d, cpa = %d, slice = %v\n", len(nums1), cap(nums1), nums1)
	// cap容量应满时，调用append  cap会翻倍
	nums1 = append(nums1, 1)
	fmt.Printf("len = %d, cpa = %d, slice = %v\n", len(nums1), cap(nums1), nums1)
}
