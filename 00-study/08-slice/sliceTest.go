package main

import "fmt"

func main() {
	myArray := []int{1, 2, 3, 4}
	fmt.Printf("myArray type is %T\n", myArray)
	fmt.Printf("value %d\t", len(myArray))
	setArrValue(myArray)
	fmt.Println(myArray)

	arr := make([]int, 3)
	arr[0] = 1
	fmt.Println(len(arr))
	fmt.Println(cap(arr))
}

// 不设置数组的大小， 使用的是引用传递
func setArrValue(arr []int) {
	arr[1] = 100
}