package main

import "fmt"

// 函数组成: func 函数名(参数列表) (返回值列表) { 函数体 }
// 参数传递方式是值传递
func foo1(a string, b int) int {
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	c := 100
	return c
}
// 匿名的多个返回值
func foo2(a string, b int) (int, int) {
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	return 666, 777
}

// 有形参名称的多个返回值
func foo3(a string, b int) (r1 int, r2 int) {
	fmt.Println("-------foo3------")
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	fmt.Println("r1 = ", r1)
	fmt.Println("r2 = ", r2)
	// 给有名称的返回值变量赋值
	r1 = 200
	r2 = 201
	return
}
// 有形参名称的多个返回值，同类型简写
func foo4(a string, b int) (r1, r2 int) {
	fmt.Println("------foo4-----")
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	r1 = 300
	r2 = 301
	return
}

func main() {
	fmt.Println("test func ")
	foo1Result := foo1("test func call", 1)
	fmt.Println("foo1 result = ", foo1Result)
	ret1, ret2 := foo2("hello", 555)
	fmt.Println("ret1 = ", ret1, " ret2 = ", ret2)
	ret3, ret4 := foo3("func3", 400)
	fmt.Println("ret1 = ", ret3, "ret4 = ", ret4)
	ret5, ret6 := foo4("hi", 333)
	fmt.Println("ret5 = ", ret5, "ret6 = ", ret6)
}
