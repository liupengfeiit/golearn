package main

import (
	"fmt"
	"strings"
)

func main() {
	var c1 complex64
	c1 = 1 + 2i
	var c2 complex128
	c2 = 2 + 3i
	fmt.Println(c1)
	fmt.Println(c2)
	c3 := "11"
	fmt.Println(len(c3))
	fmt.Println(strings.Split(c3, ""))

	s1 := "tal"
	s2 := "好未来"
	fmt.Printf("s1:%s ,s2:%s", s1, s2)

	s3 := `
		第一行
		第一行
		第一行
	`
	fmt.Println(s3)
	fmt.Println(len(s3))
	s1 = s1 + s2
	fmt.Println(s1)
	s1 += s2
	fmt.Println(s1)
	s1 = fmt.Sprintf(s1, s2)
	fmt.Println(s1)
	fmt.Println(strings.Contains(s1, s2))
	fmt.Println(strings.HasSuffix(s1, "11"))
	fmt.Println(strings.HasPrefix(s1, "tal"))
	fmt.Println(strings.Index(s1, "好"))
	fmt.Println(strings.LastIndex(s1, "好"))
	s4 := []string{"1", "2", "3"}
	fmt.Println(strings.Join(s4, "&"))
	for index, elem := range s1 {
		fmt.Printf("index: %d, elem: %c \n", index, elem)
	}
	for _, elem := range s1 {
		// rune
		fmt.Printf("elem: %c \n", elem)
	}
	for i := 0; i < len(s1); i++ {
		// byte
		fmt.Printf("index: %d, elem: %c \n", i, s1[i])
	}
	b1 := []byte(s1)
	b1[0] = 'p'
	fmt.Println(string(b1))
	b2 := []rune(s1)
	b2[0] = '白'
	fmt.Println(string(b2))
}
