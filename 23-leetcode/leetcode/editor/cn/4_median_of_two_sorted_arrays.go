package main

// leetcode submit region begin(Prohibit modification and deletion)
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	return median(nums1)
}

func median(nums []int) float64 {
	if len(nums)%2 == 0 {
		return float64(nums[len(nums)/2-1]+nums[len(nums)/2]) / 2.0
	}
	return float64(nums[len(nums)/2])
}

//leetcode submit region end(Prohibit modification and deletion)

func findMedianSortedArrays2(nums1 []int, nums2 []int) float64 {
	if len(nums1) == 0 {
		return median(nums2)
	}
	if len(nums2) == 0 {
		return median(nums1)
	}
	nums := make([]int, len(nums1)+len(nums2))
	var j, k int
	for i := 0; i < len(nums); i++ {
		if j == len(nums1) {
			for ; k < len(nums2); k++ {
				nums[i] = nums2[k]
				i++
			}
			break
		}
		if k == len(nums2) {
			for ; j < len(nums1); j++ {
				nums[i] = nums1[j]
				i++
			}
			break
		}
		if nums1[j] < nums2[k] {
			nums[i] = nums1[j]
			j++
			continue
		}
		nums[i] = nums2[k]
		k++
	}
	return median(nums)
}

func findMedianSortedArrays1(nums1 []int, nums2 []int) float64 {
	m := len(nums1)
	n := len(nums2)
	nums := make([]int, m+n)
	if m == 0 {
		if n%2 == 0 {
			return float64(nums2[n/2-1]+nums2[n/2]) / 2.0
		} else {
			return float64(nums2[n/2])
		}
	}
	if n == 0 {
		if m%2 == 0 {
			return float64(nums1[m/2-1]+nums1[m/2]) / 2.0
		} else {
			return float64(nums1[m/2])
		}
	}
	var count, i, j int
	for count != (m + n) {
		if i == m {
			for j != n {
				nums[count] = nums2[j]
				count++
				j++
			}
			break
		}
		if j == n {
			for i != m {
				nums[count] = nums1[i]
				count++
				i++
			}
			break
		}
		if nums1[i] < nums2[j] {
			nums[count] = nums1[i]
			count++
			i++
		} else {
			nums[count] = nums2[j]
			count++
			j++
		}
	}
	if count%2 == 0 {
		return float64(nums[count/2-1]+nums[count/2]) / 2.0
	}
	return float64(nums[count/2])
}
