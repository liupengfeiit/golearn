package main

func main() {
	longestPalindrome("ac")
}

// leetcode submit region begin(Prohibit modification and deletion)
func longestPalindrome(s string) string {
	slen := len(s)
	if slen < 2 {
		return s
	}
	bs := []byte(s)
	if slen == 2 {
		if bs[0] == bs[1] {
			return s
		}
		return string(bs[0])
	}
	maxLen := 1
	begin := 0
	// bp[i][j] 标识s[i...j]是否为回文串
	bp := make([][]bool, slen)
	// 初始化：所有长度为1的子串都是回文串
	for i := 0; i < slen; i++ {
		bp[i] = make([]bool, slen)
		bp[i][i] = true
	}
	// 先枚举子串长度
	for l := 2; l <= slen; l++ {
		// 枚举左边界，左边界的上限可以宽松点
		for i := 0; i < slen; i++ {
			// 由l和i确定右边界
			j := l + i - 1
			if j >= slen {
				break
			}
			if bs[i] != bs[j] {
				bp[i][j] = false
			} else {
				if j-i < 3 {
					bp[i][j] = true
				} else {
					bp[i][j] = bp[i+1][j-1]
				}
			}
			// 只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
			if bp[i][j] && j-i+1 > maxLen {
				maxLen = j - i + 1
				begin = i
			}
		}
	}
	return string(bs[begin : begin+maxLen])
}

//leetcode submit region end(Prohibit modification and deletion)
