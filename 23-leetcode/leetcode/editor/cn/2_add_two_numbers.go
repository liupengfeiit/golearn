package main

type ListNode struct {
	Val  int
	Next *ListNode
}

//leetcode submit region begin(Prohibit modification and deletion)

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	if l1.Val == 0 && l2.Val == 0 && l1.Next == nil && l2.Next == nil {
		return &ListNode{0, nil}
	}
	// return add(l1, l2, 0)
	return add1(l1, l2)

}

func add(l1 *ListNode, l2 *ListNode, a int) *ListNode {
	num := l1.Val + l2.Val + a
	l1 = l1.Next
	l2 = l2.Next
	if l1 == nil && l2 == nil && num == 0 {
		return nil
	}
	if l1 == nil {
		l1 = &ListNode{}
	}
	if l2 == nil {
		l2 = &ListNode{}
	}
	var next *ListNode
	val := num % 10
	car := num / 10
	next = add(l1, l2, car)

	return &ListNode{
		Val:  val,
		Next: next,
	}
}

func add1(l1 *ListNode, l2 *ListNode) *ListNode {
	sentinel := &ListNode{}
	cur := sentinel
	car := 0
	for {
		if l1 == nil && l2 == nil {
			break
		}
		sum := car
		if l1 != nil {
			sum += l1.Val
		}
		if l2 != nil {
			sum += l2.Val
		}
		car = sum / 10
		sum %= 10
		cur.Next = &ListNode{Val: sum}
		cur = cur.Next
		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
	}
	if car == 1 {
		cur.Next = &ListNode{Val: 1}
	}
	return sentinel.Next
}

//leetcode submit region end(Prohibit modification and deletion)
