package main

// leetcode submit region begin(Prohibit modification and deletion)
func twoSum(nums []int, target int) []int {
	dict := make(map[int]int, len(nums))
	for i, num := range nums {
		if n, ok := dict[target-num]; ok {
			return []int{i, n}
		}
		dict[num] = i
	}
	return nil
}

//leetcode submit region end(Prohibit modification and deletion)

// 暴力解法,  时间复杂度O(n2)
func twoSum1(nums []int, target int) []int {
	n := len(nums)
	result := make([]int, 2)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			if (nums[i] + nums[j]) == target {
				result[0] = i
				result[1] = j
				return result
			}
		}
	}
	return result
}
