package main

// leetcode submit region begin(Prohibit modification and deletion)
func lengthOfLongestSubstring(s string) int {
	if len(s) < 2 {
		return len(s)
	}
	cache := make(map[byte]int, len(s))
	bys := []byte(s)
	max := 0
	right := 0
	for i, _ := range bys {
		for right < len(s) && cache[s[right]] == 0 {
			cache[s[right]]++
			right++
		}
		max = Max(max, right-i)
		delete(cache, s[i])
	}
	return max
}

func Max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

//leetcode submit region end(Prohibit modification and deletion)
