package main

import "fmt"

func main() {
	fmt.Println(lengthOfLIS([]int{10, 9, 2, 5, 3, 7, 101, 18, 6, 4}))
}

func lengthOfLIS(nums []int) int {
	if len(nums) <= 1 {
		return len(nums)
	}
	dp := make([]int, len(nums))
	// 初始化就是边界情况
	dp[0] = 1
	maxAns := 1
	// 自底向上遍历
	for i := 1; i < len(nums); i++ {
		dp[i] = 1
		for j := 0; j < i; j++ {
			// //找到前面比nums[i]小的数nums[j],即有dp[i]= dp[j]+1
			if nums[i] > nums[j] {
				// 因为会有多个小于nums[i]的数，也就是会存在多种组合了嘛，我们就取最大放到dp[i]
				dp[i] = maxInt(dp[i], dp[j]+1)
			}
		}
		// 求出dp[i]后，dp最大那个就是nums的最长递增子序列
		maxAns = maxInt(maxAns, dp[i])
	}
	return maxAns
}

func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}
