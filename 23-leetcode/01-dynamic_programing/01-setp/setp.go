package main

import (
	"fmt"
	"time"
)

// 时间复杂度 O(1) * O(2^n) = O(2^n) 指数级 爆炸增长的
func numsWay(n int) int {
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	return numsWay(n-1) + numsWay(n-2)
}

var cache map[int]int = make(map[int]int)

// 时间复杂度O(n) 空间复杂度是O(n)
func numsWay1(n int) int {
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	if cache[n] > 0 {
		return cache[n]
	}
	cache[n] = numsWay1(n-1) + numsWay1(n-2)
	return cache[n]
}

func numsWay2(n int) int {
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	a, b := 1, 1
	temp := 0
	for i := 3; i <= n; i++ {
		temp = a + b
		a = b
		b = temp
	}
	return temp
}

func main() {
	n := 30
	start := time.Now().UnixMicro()
	result := numsWay(n)
	end := time.Now().UnixMicro()
	fmt.Printf("耗时：%d us, 结果： %d\n", end-start, result)

	start = time.Now().UnixMicro()
	result = numsWay1(n)
	end = time.Now().UnixMicro()
	fmt.Printf("耗时：%d us, 结果： %d\n", end-start, result)

	start = time.Now().UnixMicro()
	result = numsWay2(n)
	end = time.Now().UnixMicro()
	fmt.Printf("耗时：%d us, 结果： %d\n", end-start, result)
}
