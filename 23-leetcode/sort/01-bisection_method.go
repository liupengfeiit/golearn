package sort

// 二分法

// 给一个[]int,求最大值

func findMax(arr []int) int {
	return findGetMax(arr, 0, len(arr)-1)
}

func findGetMax(arr []int, left int, right int) int {
	if left > right-2 {
		return intMax(arr[left], arr[right])
	}
	mid := (left + right) / 2
	leftMax := findGetMax(arr, left, mid)
	rightMax := findGetMax(arr, mid+1, right)
	return intMax(leftMax, rightMax)
}

func intMax(a, b int) int {
	if a > b {
		return a
	}
	return b
}
