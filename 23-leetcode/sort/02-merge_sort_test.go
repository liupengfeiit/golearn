package sort

import (
	"fmt"
	"testing"
)

func TestMergeSort(t *testing.T) {
	arr := []int{2, 5, 8, 9, 10, 4, 3, 16, 1, 7, 8}
	result := mergeSort(arr)
	fmt.Printf("%+v\n", result)
}
