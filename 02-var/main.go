package main

import "fmt"

var (
	name string // ""
	age  int    // 0
	flag bool   // false
)

func main() {
	name = "张三"
	age = 18
	flag = true
	fmt.Println(name)
	fmt.Println(age)
	fmt.Println(flag)
	fmt.Printf("name:%s,age:%d,flag:%t", name, age, flag) // %s 字符串占位符 %d十进制占位符， %t bool占位符
	var test string
	// go 程序中声明的变量必须使用, 否则编译不通过
	test = "11"
	fmt.Println(test)
	var s string = "ts"
	fmt.Println(s)
	var s1 = "ts" // 类型推导
	fmt.Println(s1)
	s3 := 1
	fmt.Println(s3)
	// 匿名变量 使用 _ 表示, 不占用命名空间, 不会分配内存
}
