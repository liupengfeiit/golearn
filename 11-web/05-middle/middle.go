package main

import (
	"fmt"
	"log"
	"net/http"
)

// 中间件

// 这个例子将展示如何在 Go 中创建基本的日志中间件。

// 包装

func logging(f http.HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		log.Println(request.URL.Path)
		f(writer, request)
	}
}

func foo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "foo\n")
}

func bar(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "bar\n")
}

func main() {
	http.HandleFunc("/foo", logging(foo))
	http.HandleFunc("/bar", logging(bar))
	http.ListenAndServe(":8090", nil)
}
