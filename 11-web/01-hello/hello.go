package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/hello", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "Hello, you've requested: %s\n", req.URL.Path)
	})
	http.ListenAndServe(":8090", nil)
}
