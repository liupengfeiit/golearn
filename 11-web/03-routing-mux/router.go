package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

// using gorilla/mux

// go get -u github.com/gorilla/mux
// /books/{title}/page/{page}
func main() {
	r := mux.NewRouter()
	r.HandleFunc("/books/{title}/page/{page}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		fmt.Fprintf(writer, "vars[title]:%s, vars[page]:%s", vars["title"], vars["page"])
	})

	// 为特定的方法绑定 请求方法 GET/POST/PUT/DELETE
	r.HandleFunc("/books/{title}", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "create book vars[title]:%s", vars["title"])
	}).Methods("POST")
	r.HandleFunc("/books/{title}", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "get book vars[title]:%s", vars["title"])
	}).Methods("GET")
	r.HandleFunc("/books/{title}", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "update book vars[title]:%s", vars["title"])
	}).Methods("PUT")
	r.HandleFunc("/books/{title}", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "delete book vars[title]:%s", vars["title"])
	}).Methods("DELETE")
	// 将请求处理程序限制为特定的主机名或子域。
	r.HandleFunc("/books/{title}", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "hosts book vars[title]:%s", vars["title"])
	}).Host("www.mybookstore.com")
	// 将请求处理程序限制为 http/https。
	r.HandleFunc("/secure", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "https book vars[title]:%s", vars["title"])
	}).Schemes("https")
	r.HandleFunc("/insecure", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "http book vars[title]:%s", vars["title"])
	}).Schemes("http")
	// 将请求处理程序限制为特定的路径前缀。
	bookrouter := r.PathPrefix("/books").Subrouter()
	bookrouter.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "http book vars[title]:%s", vars["title"])
	})

	bookrouter.HandleFunc("/{title}", func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		fmt.Fprintf(w, "http book vars[title]:%s", vars["title"])
	})
	http.ListenAndServe(":8090", r)
}
