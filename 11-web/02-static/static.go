package main

import (
	"fmt"
	"net/http"
)

// 静态资源文件夹配置

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "test static dir ")
	})
	fs := http.FileServer(http.Dir("static/"))
	// /s/1.html  寻址  /static/1.html  http.StripPrefix 是将路径给替换为静态资源处理器
	http.Handle("/s/", http.StripPrefix("/s/", fs))
	http.ListenAndServe(":8090", nil)
}
