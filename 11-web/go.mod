module gitee.com/liupengfei_it/golearn/11-web

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)
