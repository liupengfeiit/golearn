package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

// go use mysql

// go get -u github.com/go-sql-driver/mysql
func main() {
	db, err := sql.Open("mysql", "root:123456@(127.0.0.1:3306)/test?parseTime=true")
	if err != nil {
		log.Fatal(err)
	}
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	{
		// create a new table
		createSql := `CREATE TABLE users (
                id INT AUTO_INCREMENT,
                username TEXT NOT NULL,
                password TEXT NOT NULL,
                created_at DATETIME,
                PRIMARY KEY (id)
            );`
		if _, err := db.Exec(createSql); err != nil {
			log.Fatal(err)
		}
	}
	{
		// insert a new user
		inserSql := `INSERT INTO users (username, password, created_at) VALUES (?, ?, ?)`
		username := "张三"
		password := "password123456"
		createdAt := time.Now()
		result, err := db.Exec(inserSql, username, password, createdAt)
		if err != nil {
			log.Fatal(err)
		}
		id, err := result.LastInsertId()
		fmt.Println("insert success, id : ", id)
	}
	{
		// query a single user
		var (
			id        int
			username  string
			password  string
			createdAt time.Time
		)
		querySql := "select id, username, password, created_at from users where id = ? "
		if err := db.QueryRow(querySql, 1).Scan(&id, &username, &password, &createdAt); err != nil {
			log.Fatal(err)
		}
		fmt.Println(id, username, password, createdAt)
	}

	{
		// query all users
		type user struct {
			id        int
			username  string
			password  string
			createdAt time.Time
		}
		rows, err := db.Query("select id, username, password, created_at from users")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		var users []user
		for rows.Next() {
			var u user
			err := rows.Scan(&u.id, &u.username, &u.password, &u.createdAt)
			if err != nil {
				log.Fatal(err)
			}
			users = append(users, u)
		}
		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%#v", users)
	}

	{
		// delete row
		_, err := db.Exec("delete from users where id = ?", 1)
		if err != nil {
			log.Fatal(err)
		}
	}

}
