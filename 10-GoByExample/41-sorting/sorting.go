package main

import (
	"fmt"
	"sort"
)

// Go 的sort包实现了对内置类型和用户定义类型的排序。我们将首先查看内置函数的排序。
// sort 排序包 , sort.Strings() sort.Ints() 升序
func main() {
	strs := []string{"c", "a", "b"}
	sort.Strings(strs)
	fmt.Println("Strings:", strs)
	sort.Sort(sort.Reverse(sort.StringSlice(strs))) // 倒序
	fmt.Println("Strings:", strs)

	ints := []int{7, 2, 4}
	sort.Ints(ints)
	fmt.Println("ints: ", ints)
	s := sort.IntsAreSorted(ints) // 是否已经排序了
	fmt.Println("sored: ", s)

}
