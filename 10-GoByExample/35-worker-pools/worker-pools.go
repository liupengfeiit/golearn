package main

import (
	"fmt"
	"time"
)

// 在这个例子中，我们将看看如何使用 goroutines 和通道来实现一个工作池。

// 复用goroutine goroutine 池

// jobs 只写的任务通道, results 只写的结果通道
// jobs是一个带缓冲去的通道
func worker(id int, jobs <-chan int, results chan<- int) {
	// 当通道中没有关闭并且还有数据时会一直执行, 等价于下述代码
	for j := range jobs {
		fmt.Println("worker", id, "started job", j)
		time.Sleep(time.Second)
		fmt.Println("worker", id, "finished job", j)
		results <- j * 2
	}
	/*for {
		// 通道中没有数据了
		j, more := <-jobs
		if !more {
			return
		}
		fmt.Println("worker", id, "started job", j)
		time.Sleep(time.Second)
		fmt.Println("worker", id, "finished job", j)
		results <- j * 2
	}*/
}

func main() {
	const numJobs = 5
	jobs := make(chan int, numJobs)
	results := make(chan int, numJobs)

	for w := 1; w <= 3; w++ {
		go worker(w, jobs, results)
	}

	for j := 1; j <= numJobs; j++ {
		jobs <- j
	}

	close(jobs)
	for a := 1; a <= numJobs; a++ {
		<-results
	}
}
