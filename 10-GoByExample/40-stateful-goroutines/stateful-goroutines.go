package main

import (
	"fmt"
	"math/rand"
	"sync/atomic"
	"time"
)
// 有状态的goroutines
// 1. 39-mutexes 带有互斥锁的显式锁定 来同步跨多个 goroutine 对共享状态的访问
// 2. 使用 goroutine 和通道的内置同步功能来实现相同的结果
// 这种基于通道的方法与 Go 的共享内存的想法一致，通过通信和让每条数据恰好由 1 个 goroutine 拥有。
// 总结：sync.Mutexes 类似于java中的锁,
// 通过通道来实现同步的效果,类似于AQS阻塞队列,生产者消费者模型来实现 end

// 读操作
type readOp struct {
	key  int      // 读的key
	resp chan int // 操作完成后,响应的channel
}

// 写操作
type writeOp struct {
	key  int       // 写的key
	val  int       // 写的值
	resp chan bool // 操作完成后,响应的channel
}

func main() {
	var readOps uint64  // 读操作次数
	var writeOps uint64 // 写操作次数

	reads := make(chan readOp)   // 存放读操作的通道
	writes := make(chan writeOp) // 存放写操作的通道
	// 开启goroutine, 定义状态map,也就是读和写操作的 对象
	// 死循环, 使用 select 监听 读操作的通道和写操作的通道, 来处理 状态state
	// 因为 读,写通道本身是阻塞顺序执行的,所以可以保证state（竞态资源）不会有线程安全问题
	go func() {
		// 操作并发共享的状态
		var state = make(map[int]int)
		for {
			select {
			case read := <-reads:
				read.resp <- state[read.key]
			case write := <-writes:
				state[write.key] = write.val
				write.resp <- true
			}
		}
	}()
	// 开启100个读操作的goroutine
	// 执行顺序,读操作 放入通道中, 维护state的goroutine 从通道中监听到有消息写入，执行对应的操作
	for r := 0; r < 100; r++ {
		go func() {
			for {
				read := readOp{
					key:  rand.Intn(5),
					resp: make(chan int),
				}
				reads <- read // 读操作放入 读通道中, 并发 会产生阻塞
				<-read.resp // 拿到读操作的响应结果
				atomic.AddUint64(&readOps, 1) // 读操作次数加1,因为 读操作次数会产生并发修改，所以使用atomic原子修改
				time.Sleep(time.Millisecond)
			}
		}()
	}
	// 开启10个写操作的goroutine
	// 执行顺序,写操作 放入通道中, 维护state的goroutine 从通道中监听到有消息写入，执行对应的操作
	for w := 0; w < 10; w++ {
		go func() {
			for {
				write := writeOp{
					key:  rand.Intn(5),
					val:  rand.Intn(100),
					resp: make(chan bool),
				}
				writes <- write  // 写操作放入 读通道中, 并发 会产生阻塞
				<-write.resp
				atomic.AddUint64(&writeOps, 1)
				time.Sleep(time.Millisecond)
			}
		}()
	}

	time.Sleep(time.Second)
	readOpsFinal := atomic.LoadUint64(&readOps)
	fmt.Println("readOps:", readOpsFinal)
	writeOpsFinal := atomic.LoadUint64(&writeOps)
	fmt.Println("writeOps:", writeOpsFinal)
}
