package main

import "fmt"

// 默认情况下，发送和接收会阻塞，直到发送方和接收方都准备就绪。这个属性允许我们在程序结束时等待"ping" 消息，而不必使用任何其他同步。
func main() {
	message := make(chan string)
	go func() {
		message <- "ping"
	}()
	msg := <-message
	fmt.Println(msg)
}
