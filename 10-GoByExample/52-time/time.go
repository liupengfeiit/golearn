package main

import (
	"fmt"
	"time"
)

func main() {
	p := fmt.Println

	now := time.Now()

	p(now)
	then := time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)
	p(then)

	p(then.Year())
	p(then.Month())
	p(then.Day())
	p(then.Hour())
	p(then.Minute())
	p(then.Second())
	p(then.Nanosecond())
	p(then.Location())
	p(then.Weekday())

	p(then.Before(now))
	p(then.After(now))
	p(then.Equal(now))

	diff := now.Sub(then)
	p(diff)

	p(diff.Hours())
	p(diff.Minutes())
	p(diff.Seconds())
	p(diff.Nanoseconds())

	p(then.Add(diff))
	p(then.Add(-diff))
	// 24小时内时间计算
	t1, _ := time.ParseDuration("1h")
	p(t1)
	p(now.Add(t1))
	t2, _ := time.ParseDuration("-1h")
	p(t2)
	p(now.Add(t2))

	t3, _ := time.ParseDuration("1h1m1s")
	p(t3)
	p(now.Add(t3))

	t4, _ := time.ParseDuration("10m")
	p(t4)
	p(now.Add(t4))

	m3 := now.Add(t3)
	sub1 := now.Sub(m3)
	p(sub1)
	p(sub1.Hours())
	p(sub1.Minutes())
	p(sub1.Seconds())

	// 返回当前时间与 t 的时间差，返回值是 Duration
	// time.Since(t Time) Duration

	// 返回 t 与当前时间的时间差，返回值是 Duration
	// time.Until(t Time) Duration

	t5, _ := time.ParseDuration("-1h")
	m1 := now.Add(t5)
	fmt.Println(m1)
	fmt.Println(time.Since(m1))
	fmt.Println(time.Until(m1))

	// 24小时之外的时间计算  time.AddDate()
	// func (t Time) AddDate(years int, months int, days int) Time

	// 一年一个月零一天 之后的时间
	m2 := now.AddDate(1, 1, 1)
	fmt.Println(m2)
	// 日期的比较总共有三种：之前、之后和相等。
	// 如果 t 代表的时间点在 u 之前，返回真；否则返回假。
	//func (t Time) Before(u Time) bool

	// 如果 t 代表的时间点在 u 之后，返回真；否则返回假。
	//func (t Time) After(u Time) bool

	// 比较时间是否相等，相等返回真；否则返回假。
	//func (t Time) Equal(u Time) bool

	fmt.Println(time.Now().Unix())
}
