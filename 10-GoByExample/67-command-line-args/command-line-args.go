package main

import (
	"fmt"
	"os"
)

// 获取命令 带的参数

func main() {
	argsWithProg := os.Args

	argsWithoutProg := os.Args[1:]

	arg := os.Args[3]

	fmt.Println(argsWithProg)
	fmt.Println(argsWithoutProg)
	fmt.Println(arg)
}
