package main

import (
	"fmt"
)


// 使用  select-default 来不阻塞等待通道 实现非阻塞发送、接收，甚至非阻塞多路selects。

func main() {
	message := make(chan string) // 不带缓冲区的channel, 发送数据时，需要等接收方接收数据，才解除阻塞， 接收数据时同理
	signal := make(chan bool, 1) // 带缓冲区的channel, 发送数据时，如果缓冲区没有满， 则不阻塞， 否则会阻塞等待接收
	select {
	case msg := <-message:
		fmt.Println("received message:", msg)
	default:
		fmt.Println("no message received")
	}

	msg := "hi"
	select {
	case message <- msg: // 不等待阻塞发送, 执行了默认情况
		fmt.Println("send message:", msg)
	default:
		fmt.Println("no message sent")
	}

	signal <- false // 由于signal 是带缓存的channel 所以不阻塞， 否则会出现死锁， 需要 goroutine运行，

	select {
	case msg := <- message:
		fmt.Println("received message:", msg)
	case sig := <-signal:
		fmt.Println("received signal:", sig)
	default:
		fmt.Println("no activity")
	}
}
