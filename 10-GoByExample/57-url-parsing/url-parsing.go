package main

import (
	"fmt"
	"net"
	"net/url"
)
// url 解析
func main() {

	s := "postgres://user:pass@host.com:5432/path?k=v#f"

	u, err := url.Parse(s)
	if err != nil {
		panic(err)
	}
	// 协议
	fmt.Println(u.Scheme)
	// 用户
	fmt.Println(u.User)
	// 用户名
	fmt.Println(u.User.Username())
	// 密码
	p, _ := u.User.Password()
	fmt.Println(p)
	// ip port
	fmt.Println(u.Host)
	// 拆分域名和端口
	host, port, _ := net.SplitHostPort(u.Host)
	fmt.Println(host)
	fmt.Println(port)
	// 访问的资源路径 /path
	fmt.Println(u.Path)
	// f
	fmt.Println(u.Fragment)
	// 参数 k=v
	fmt.Println(u.RawQuery)
	m, _ := url.ParseQuery(u.RawQuery)
	fmt.Println(m)
	fmt.Println(m["k"][0])
}