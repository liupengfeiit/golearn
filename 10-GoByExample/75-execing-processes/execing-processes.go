package main

import (
	"os"
	"os/exec"
	"syscall"
)

// 用另一个（可能是非 Go）进程完全替换当前的 Go 进程。
func main() {

	binary, lookErr := exec.LookPath("ls")
	if lookErr != nil {
		panic(lookErr)
	}

	args := []string{"ls", "-a", "-l", "-h"}
	// 获取系统的环境变量
	env := os.Environ()

	execErr := syscall.Exec(binary, args, env)
	if execErr != nil {
		panic(execErr)
	}
}
