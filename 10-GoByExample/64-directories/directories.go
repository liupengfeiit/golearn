package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// 遍历文件夹所有的文件 filepath.Walk(p, visit)
func main() {
	p := "/Users/liupengfei6/Downloads/8小时转职Golang工程师/subdir"
	err := os.Mkdir(p, 0755)
	check(err)
	defer os.RemoveAll(p)
	createEmptyFile := func(name string) {
		d := []byte("")
		check(os.WriteFile(name, d, 0644))
	}
	createEmptyFile(p + "/file1")
	err = os.MkdirAll(p+"/parent/child", 0755)
	check(err)

	createEmptyFile(p + "/parent/file2")
	createEmptyFile(p + "/parent/file3")
	createEmptyFile(p + "/parent/child/file4")
	c, err := os.ReadDir(p + "/parent")
	check(err)
	fmt.Println("listing " + p + "/parent")
	for _, entry := range c {
		fmt.Println(" ", entry.Name(), entry.IsDir())
	}
	err = os.Chdir("../../../")
	check(err)
	fmt.Println("visiting subdir")
	err = filepath.Walk(p, visit)
}

func visit(p string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}
	fmt.Println(" ", p, info.IsDir())
	return nil
}
