package main

import "fmt"

// 这个例子还表明可以关闭一个非空通道，但仍然可以接收剩余的值。
// 通过 for range 遍历通道中的元素
func main() {
	queue := make(chan string, 2)
	queue <- "one"
	queue <- "two"

	close(queue)
	// 遍历通道，直到通道关闭,并且数据处理完毕，等价于下述的逻辑
	for elem := range queue {
		fmt.Println(elem)
	}
	/*for {
		elem, more := <-queue
		if !more {
			return
		}
		fmt.Println(elem)
	}*/

}
