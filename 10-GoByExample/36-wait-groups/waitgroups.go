package main

import (
	"fmt"
	"sync"
	"time"
)

// 要等待多个 goroutine 完成，我们可以使用等待组。
// 类似于currentDownLatch
func worker(id int) {
	fmt.Printf("Worker %d starting\n", id)
	time.Sleep(time.Second)
	fmt.Printf("Worker %d done\n", id)
}

func main() {
	var wg sync.WaitGroup
	// wg.Add(5); // 计数器加5,
	for i := 1; i <= 5; i++ {
		// 等待组加1 计数器加1
		wg.Add(1)
		j := i
		go func() {
			// 任务执行完成，等待组减1 计数器减1
			defer wg.Done()
			worker(j)
		}()
	}
	wg.Wait()
	fmt.Println("end")
}