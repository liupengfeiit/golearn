package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	// 方式1： os.WriteFile 文件不存在会进行创建
	d1 := []byte("hello\ngo\n")
	// perm 文件不存在时,创建文件的权限
	err := os.WriteFile("/Users/liupengfei6/Downloads/8小时转职Golang工程师/testFile.txt", d1, 0644)
	check(err)

	// 方式2： 先创建文件，再操作写入
	f, err := os.Create("/Users/liupengfei6/Downloads/8小时转职Golang工程师/testFile1.txt")
	check(err)
	defer f.Close()

	d2 := []byte{115, 111, 109, 101, 10}
	// 2.1 文件.write 直接写入
	n2, err := f.Write(d2)
	check(err)
	fmt.Printf("wrote %d bytes\n", n2)

	n3, err := f.WriteString("writes\n")
	check(err)
	fmt.Printf("wrote %d bytes\n", n3)
	// 发出同步以刷新对稳定存储的写操作。
	f.Sync()
	// 2.2 文件的缓冲io进行写入
	w := bufio.NewWriter(f)
	n4, err := w.WriteString("buffered\n")
	check(err)
	fmt.Printf("wrote %d bytes\n", n4)
	w.Flush()
}
