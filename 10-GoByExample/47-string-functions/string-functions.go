package main

import (
	"fmt"
	"strings"
)

var p = fmt.Println

func main() {
	// 字符串内是否包含子串
	p("Contains:", strings.Contains("test", "es"))
	// 字符串出现子串的数量
	p("Count:", strings.Count("test", "t"))
	// 字符串的前缀是否是子串
	p("HasPrefix:", strings.HasPrefix("test", "te"))
	// 字符串的后缀是否是子串
	p("HasSuffix:", strings.HasSuffix("test", "st"))
	// 字符串第一次出现子串的位置
	p("Index:", strings.Index("test", "e"))
	// 把切片转换为 字符串,以子串作为分隔符
	p("Join:", strings.Join([]string{"a", "b"}, "-"))
	// 将字符串重复拼接n次
	p("Repeat:", strings.Repeat("a", 5))
	// 将所有的字符串的子串替换  replaceAll
	p("Replace:", strings.Replace("foo", "o", "0", -1))
	// 将字符串的n个子串替换
	p("Replace:", strings.Replace("foo", "o", "0", 1))
	// 对字符串以子串进行分隔成切片
	p("Split:", strings.Split("a-b-c-d-e", "-"))
	// 字符串转小写
	p("ToLower:", strings.ToLower("TEST"))
	// 字符串转大写
	p("ToUpper:", strings.ToUpper("test"))
	p()
	// 字符串长度
	p("Len:", len("hello"))
	// 字符串转char
	p("char:", "hello"[1])
}
