package main

import (
	"fmt"
	"time"
)

// tickers 定时重复做一件事情

func main() {
	ticker := time.NewTicker(500 * time.Millisecond)
	// 终止标记通道
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C: // 定时器的通道, 获取时间
				fmt.Println("tick at", t)
			}
		}
	}()
	time.Sleep(1600 * time.Millisecond)
	// Stop关闭定时器。停止后，将不再发送蜱。
	// 停止不关闭通道，以防止并发goroutine
	ticker.Stop()
	done <- true
	fmt.Println("ticker stopped")
}
