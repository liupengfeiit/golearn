package main

import (
	"fmt"
	"strings"
)
// 集合中的方法, anyMatch AllMatch Filter
// 缺少lambda表达式, 具备方法引用
func main() {
	var strs = []string{"peach", "apple", "pear", "plum"}
	fmt.Println(Index(strs, "pear"))
	fmt.Println(Include(strs, "grape"))
	// 是否包含前缀是 p的元素
	fmt.Println(Any(strs, func(v string) bool {
		return strings.HasPrefix(v, "p")
	}))
	// 所有元素前缀都为p
	fmt.Println(All(strs, func(v string) bool {
		return strings.HasPrefix(v, "p")
	}))
	// 筛选包含e的元素
	fmt.Println(Filter(strs, func(v string) bool {
		return strings.Contains(v, "e")
	}))
	// 元素全部转为大写
	fmt.Println(Map(strs, strings.ToUpper))
}


// Index 查找元素在切片中的位置
func Index(vs []string, t string) int {
	for i, v := range vs {
		if v == t {
			return i
		}
	}
	return -1
}

// Include 判断 元素在切片中是否存在
func Include(vs []string, t string) bool {
	return Index(vs, t) > -1
}

// Any f 谓词, 在切片中满足 f 谓词的结果
func Any(vs []string, f func(string) bool) bool {
	for _, v := range vs {
		if f(v) {
			return true
		}
	}
	return false
}

// All f 谓词, 在切片中全部满足的f谓词的结果
func All(vs[] string, f func(string) bool) bool {
	for _, v := range vs {
		if !f(v) {
			return false
		}
	}
	return true
}

// Filter f谓词, 在切片中筛选满足f谓词的结果
func Filter(vs []string, f func(string) bool) []string {
	vsf := make([]string, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

// Map 将数据通过f执行转换成其他数据  a -> b
func Map(vs []string, f func(string) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

