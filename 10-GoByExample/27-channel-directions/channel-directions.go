package main

import "fmt"

// channel 方向 使用通道作为函数参数时，您可以指定通道是否仅用于发送或接收值。这种特殊性增加了程序的类型安全性。
// 此ping函数仅接受用于发送值的通道。尝试在此通道上接收将是编译时错误。
// 该pong函数接受一个通道用于接收 ( pings) 和第二个通道用于发送 ( pongs)。
// pings chan<- string  仅将消息写入到通道中， 不能读，读编译错误

func ping(pings chan<- string, msg string) {
	pings <- msg
}

// pings <-chan string 仅用于读取消息， 不能将消息写入到通道中， 否则编译错误
func pong(pings <-chan string, pongs chan<- string) {
	msg := <-pings
	pongs <- msg
}

func pingh(pings <- chan string, msg string) {
	msg = <- pings
	fmt.Println(msg)
}

func main() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings, "passedMessage")
	pong(pings, pongs)
	fmt.Println(<-pongs)
}