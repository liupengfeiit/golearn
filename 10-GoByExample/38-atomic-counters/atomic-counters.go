package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// 原子计数器

// Go 中管理状态的主要机制是通过通道进行通信， 例如在 35-worker-pools中
// 如何使用sync/atomic包来处理由多个 goroutine 访问的原子计数器。

func main() {
	var ops uint64

	var wg sync.WaitGroup
	// 50个协程中，每个协程循环1000次，操作 计数器加一
	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func() {
			for c := 0; c < 1000; c++ {
				// 原子加1
				atomic.AddUint64(&ops, 1)
			}
			wg.Done()
		}()
	}
	wg.Wait()
	// ops = 50 * 1000, 由于是并发修改，如何保证线程安全
	// 测试 atomic 包 原子性 累加，保证结果正确
	fmt.Println("ops:", ops)
}