package main

import "fmt"
// 通过使用内置函数，Go 可以从panic中恢复recover。
// recover可以阻止 panic中止程序并让它继续执行。
//
// 例子：如果一个客户端连接出现严重错误，服务器不希望崩溃。
// 相反，服务器会想要关闭该连接并继续为其他客户端提供服务。
// 事实上，这就是 Go net/http 默认为 HTTP 服务器所做的。
func mayPanic() {
	panic("a problem")
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("recovered. error:\n", r)
		}
	}()
	mayPanic()
	fmt.Println("after myPanic()")
}
