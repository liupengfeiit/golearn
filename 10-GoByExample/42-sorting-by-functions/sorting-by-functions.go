package main

import (
	"fmt"
	"sort"
)

// 根据字符串长度 排序
// 给string 切片 起个别名
// byLength(fruits) 隐式转换
type byLength []string

// 给别名上实现Interface接口
// Len 实现Interface 接口
func (s byLength) Len() int {
	return len(s)
}

func (s byLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s byLength) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}


func main() {
	fruits := []string{"peach", "banana", "kiwi"}
	// byLength(fruits) 将 切换 转换为interface类型
	//
	sort.Sort(byLength(fruits)) //
	fmt.Println(fruits)
}

