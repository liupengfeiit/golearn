package main

import "fmt"

// channel  close
//  关闭通道表示不会在其上发送更多值。这对于将完成传达给通道的接收器很有用。
func main() {
	jobs := make(chan int, 5)
	done := make(chan bool)

	go func() {
		for {
			// j 通道读取的数据, more 是否具有更多的数据, 如果channel关闭,则more = false
			j, more := <-jobs
			if more {
				fmt.Println("received job", j)
			} else {
				fmt.Println("received all jobs")
				done <- true
				return
			}
		}
	}()

	for i := 1; i < 3; i++ {
		jobs <- i
		fmt.Println("send job", i)
	}
	close(jobs)
	fmt.Println("send all jobs")
	<-done
}
