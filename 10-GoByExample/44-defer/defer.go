package main

import (
	"fmt"
	"os"
)

// defer 方法结束后调用, 执行顺序在return之后

func main() {

	f := createFile("/Users/liupengfei6/Downloads/8小时转职Golang工程师/testFile.txt")
	defer closeFile(f)
	writeFile(f)
}


func createFile(p string) *os.File {
	fmt.Println("creating")
	f, err := os.Create(p)
	if err != nil {
		panic(err)
	}
	return f
}

func writeFile(f *os.File) {
	fmt.Println("writing")
	fmt.Fprintln(f, "data")
}

func closeFile(f *os.File) {
	fmt.Println("closing")
	err := f.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}