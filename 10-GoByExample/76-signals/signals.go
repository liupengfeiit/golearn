package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// 我们可能希望服务器在收到 . 时正常关闭SIGTERM，或者命令行工具在收到SIGINT. 这是在 Go 中使用通道处理信号的方法。
// Go 程序能够智能地处理Unix 信号。
func main() {

	sigs := make(chan os.Signal, 1)
	// 系统信号写入到 sigs 通道中
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	done := make(chan bool, 1)

	go func() {
		// 等待系统信号
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		// go程序结束信号
		done <- true
	}()

	fmt.Println("awaiting signal")
	// 等待go程序结束信号
	<-done
	fmt.Println("exiting")
}
