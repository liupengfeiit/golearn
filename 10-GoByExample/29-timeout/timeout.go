package main

import (
	"fmt"
	"time"
)

// channel 和select 超时对于连接到外部资源或需要限制执行时间的程序很重要。

func main() {
	c1 := make(chan string, 1)
	go func() {
		time.Sleep(2 * time.Second)
		c1 <- "result 1"
	}()

	select {
	case res := <-c1: // 当c1 读取到数据比下述case快的话，会重置定时器的阻塞时间
		fmt.Println(res)
	case <-time.After(1 * time.Second): // 1秒后,定时器不再阻塞
		fmt.Println("time out 1")
	}

	c2 := make(chan string, 1)
	go func() {
		time.Sleep(2 * time.Second)
		c2 <- "result 2"
	}()

	select {
	case res := <-c2:
		fmt.Println(res)
	case <- time.After(3 * time.Second):
		fmt.Println("time out 2")
	}

}
