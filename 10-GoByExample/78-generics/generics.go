package main

import (
	"fmt"
	"reflect"
	"sync"
)

func SumIntsOrFloats[K comparable, V int64 | float64](m map[K]V) V {
	var s V
	for _, v := range m {
		s += v
	}
	return s
}

type Strategy interface {
	Code() int
}

var (
	factory     map[int]map[string]Strategy
	factoryOnce sync.Once
	factoryLock sync.RWMutex
)

func init() {
	factoryOnce.Do(func() {
		factory = make(map[int]map[string]Strategy)
	})
}

func Get[S Strategy](code int, s *S) (result S) {
	factoryLock.RLock()
	defer factoryLock.RUnlock()

	m := factory[code]
	k := reflect.TypeOf((*S)(nil)).String()
	if m == nil || m[k] == nil {
		// todo 没有注册的获取, 是否程序结束
		return
	}
	result = m[k].(S)
	*s = result
	return
}

func Register[S Strategy](s S) {
	if factory[s.Code()] == nil {
		factoryLock.Lock()
		if factory[s.Code()] == nil {
			factory[s.Code()] = make(map[string]Strategy)
		}
		factoryLock.Unlock()
	}
	factory[s.Code()][reflect.TypeOf((*S)(nil)).String()] = s

}

type PlatformStrategy interface {
	Strategy
	Auth()
	GetOrder()
}

type douyin struct {
}

func (d *douyin) Code() int {
	return 1
}

func (d *douyin) Auth() {
	fmt.Println("douyin auth.....")
}

func (d *douyin) GetOrder() {
	fmt.Println("douyin get order.....")
}

func NewDouyin() (result PlatformStrategy) {
	result = &douyin{}
	Register(result)
	return
}

type tianmao struct {
}

func (t *tianmao) Code() int {
	return 2
}

func (t *tianmao) Auth() {
	fmt.Println("tianmao auth.....")
}

func (t *tianmao) GetOrder() {
	fmt.Println("tianmao get order.....")
}

func NewTianmao() (result PlatformStrategy) {
	result = &tianmao{}
	Register(result)
	return
}

type IdStrategy interface {
	Strategy
	Id() string
}

type uid struct {
}

type id struct {
}

func NewUid() (result IdStrategy) {
	result = &uid{}
	Register(result)
	return
}

func NewId() (result IdStrategy) {
	result = &id{}
	Register(result)
	return
}

func (u *uid) Code() int {
	return 1
}

func (u *uid) Id() string {
	return "uuid generate"
}

func (i *id) Code() int {
	return 2
}

func (i *id) Id() string {
	return "id auto generate"
}

func Set(i *interface{}) {
	t := 1
	*i = t
}

func main() {
	NewDouyin()
	NewTianmao()
	NewId()
	NewUid()
	fmt.Printf("%+v \n", factory)
	var platformStrategy PlatformStrategy
	Get(1, &platformStrategy)
	platformStrategy.Auth()
	platformStrategy.GetOrder()

	//var i interface{}
	//Set(&i)
	//fmt.Println(i)
	Get(2, &platformStrategy)
	platformStrategy.Auth()
	platformStrategy.GetOrder()

	var idStrategy IdStrategy
	Get(1, &idStrategy)
	fmt.Println(idStrategy.Id())

	Get(2, &idStrategy)
	fmt.Println(idStrategy.Id())
}
