package main

import (
	"flag"
	"fmt"
)

// 接收命令行参数

// go build command-line-flags.go
//  ./command-line-flags -word=opt -numb=7 -fork -svar=flag
// ./command-line-flags -word=opt
// ./command-line-flags -word=opt a1 a2 a3
// ./command-line-flags -word=opt a1 a2 a3 -numb=7
// ./command-line-flags -h
// ./command-line-flags -wat
func main() {
	wordPtr := flag.String("word", "foo", "a string")

	numbPtr := flag.Int("numb", 42, "an int")

	forkPtr := flag.Bool("fork", false, "a bool")

	var svar string
	flag.StringVar(&svar, "svar", "bar", "a string var")

	flag.Parse()

	fmt.Println("word:", *wordPtr)
	fmt.Println("numb:", *numbPtr)
	fmt.Println("fork:", *forkPtr)
	fmt.Println("svar:", svar)
	fmt.Println("tail:", flag.Args())

}
