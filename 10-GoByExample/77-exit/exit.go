package main

import (
	"fmt"
	"os"
)

// go进程退出
func main() {

	defer fmt.Println("!")
	// 系统退出，  defer不会执行
	os.Exit(3)
}
