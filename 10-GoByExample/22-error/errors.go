package main

import (
	"errors"
	"fmt"
)

func f1(arg int) (int, error) {
	if arg == 42 {
		return -1, errors.New("can't work with 42")
	}
	return arg + 3, nil
}


type argError struct {
	arg int
	prob string
}

func (e *argError) Error() string {
	return fmt.Sprintf("%d - %s", e.arg, e.prob)
}

func f2(arg int) (int, error) {
	if arg == 42 {
		return -1, &argError{arg, "can't work with it"}
	}
	return arg + 3, nil
}

func main() {
	for _, i := range []int{7, 42} {
		if r, e := f1(i); e != nil {
			fmt.Println("f1 failed:", e)
		} else {
			fmt.Println("f1 worked:", r)
		}
	}
	for _, i := range []int{7, 42} {
		if r, e := f2(i); e!= nil {
			fmt.Println("f2 failed:", e)
		} else {
			fmt.Println("f2 worked:", r)
		}
	}
	_, e := f2(42)
	// e是不是argError类型的, ae 是 e转为 argeError类型值, ok是 判断 类型是不是 argError
	// 断言机制  父类型判断是不是子类型,  error是个接口，本质上是个指针， argError 是一个结构体,
	// 指针断言是否为一个结构体，需要断言是否为结构体的指针
	if ae, ok := e.(*argError); ok {
		fmt.Println("--------")
		fmt.Println(ae)
		fmt.Println(ok)
		fmt.Println(ae.arg)
		fmt.Println(ae.prob)
	}
}