package main

import (
	"fmt"
	"sync"
)

// 互斥锁
// 原子性操作可以使用atomic
// 对于复杂的状态,使用互斥锁来跨多个goroutine安全的访问数据

type Container struct {
	mu       sync.Mutex // 互斥锁
	counters map[string]int
	o        Other
}

type Other struct {
	a int
}

// 对某一个key  安全的累加1
func (c *Container) inc(name string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.counters[name]++
}

// 开启3个goroutine 操作map,对value累加
func main() {
	c := Container{
		counters: map[string]int{"a": 0, "b": 0},
	}
	var wg sync.WaitGroup
	doIncrement := func(name string, n int) {
		for i := 0; i < n; i++ {
			c.inc(name)
		}
		wg.Done()
	}
	wg.Add(3)
	go doIncrement("a", 10000)
	go doIncrement("a", 10000)
	go doIncrement("b", 10000)

	wg.Wait()
	fmt.Println(c.counters)
}
