package main

import (
	"fmt"
	"testing"
)

// 单元测试和基准测试
// 它可以是任何包 测试代码通常与它测试的代码位于同一个包中。
// 文件以 _test.go 结尾
// 详细模式运行当前项目中的所有测试 go test -v
// 运行当前项目中的所有基准测试。所有测试都在基准测试之前运行。
// 该bench标志使用正则表达式过滤基准函数名称。go test -bench=.
func IntMin(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// t.Error*将报告测试失败但继续执行测试。t.Fatal*将报告测试失败并立即停止测试。
func TestIntMinBasic(t *testing.T) {
	ans := IntMin(2, -2)
	if ans != -2 {
		t.Errorf("IntMin(2, -2) = %d; want -2", ans)
	}
}

// 编写测试可能是重复的，因此使用table-driven style是惯用的，其中测试输入和预期输出列在一个表中，一个循环遍历它们并执行测试逻辑。
func TestIntMinTableDriven(t *testing.T) {
	var tests = []struct {
		a, b int
		want int
	}{
		{0, 1, 0},
		{1, 0, 0},
		{2, -2, -2},
		{0, -1, -1},
		{-1, 0, -1},
	}
	for _, tt := range tests {
		testname := fmt.Sprintf("%d, %d", tt.a, tt.b)
		// t.Run 允许运行“子测试”，每个表条目一个。这些在执行时单独显示go test -v。
		t.Run(testname, func(t *testing.T) {
			ans := IntMin(tt.a, tt.b)
			if ans != tt.want {
				t.Errorf("got %d, want %d", ans, tt.want)
			}
		})
	}
}

// 基准测试通常放在_test.go文件中，并以Benchmark. 该testing转轮执行每个基准功能几次，增加 b.N每次运行，直到它收集精确的测量。
func BenchmarkIntMin(b *testing.B) {
	for i := 0; i < b.N; i++ {
		IntMin(1, 2)
	}
}
