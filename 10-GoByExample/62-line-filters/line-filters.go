package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// 行过滤器是一种常见的程序类型，它读取stdin上的输入，对其进行处理，然后将一些派生结果打印到stdout。Grep和sed是常用的行过滤器。
//
// 下面是一个Go中的行筛选器示例，它将所有输入文本的大小写版本写入其中。您可以使用此模式编写自己的Go行过滤器。
// cat /tmp/lines | go run line-filters.go
func main() {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		ucl := strings.ToUpper(scanner.Text())
		fmt.Println(ucl)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	}
}
