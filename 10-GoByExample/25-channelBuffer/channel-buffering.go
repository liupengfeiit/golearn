package main

import "fmt"

// 带有缓冲区的channel, 放入的值不超过缓冲区大小 则不会阻塞, 获取值时 如果缓冲区存在值，则不会阻塞
func main() {
	messages := make(chan string, 2)
	messages <- "buffered"
	messages <- "channel"

	fmt.Println(<-messages)
	fmt.Println(<-messages)
}