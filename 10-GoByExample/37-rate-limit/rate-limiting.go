package main

import (
	"fmt"
	"time"
)

// 速率限制 是控制资源利用率和维持服务质量的重要机制。Go 优雅地支持使用 goroutines、channels 和tickers 进行速率限制。
// 速率限制

// 恒速, 用于保证服务正常运营，而不是无限制的处理,导致服务器宕机
// 缓冲限制器通道实现突发情况
// 比如服务器qps 恒速处理100, 为了应对突发情况, 支持突发qps 10, 服务的qps峰值是110


// 效果： 第一批请求 requests 每200ms处理一次
// 第二批请求 burstyRequests 突发速率，立即为前3个请求提供服务,其余两个每200ms处理一次 end

func main() {
	// 模拟, 每200ms 处理一个请求, 以固定的速率处理
	requests := make(chan int, 5)
	for i := 1; i <= 5; i++ {
		requests <- i
	}
	close(requests)

	limiter := time.Tick(200 * time.Millisecond)

	for req := range requests {
		<- limiter
		fmt.Println("request", req, time.Now())
	}

	fmt.Println("------------")
	// 允许短时间突发的请求, 通过 缓冲限制器通道来实现
	// 缓冲限制器， 设置最多3个事件的突发
	burstyLimiter := make(chan time.Time, 3)

	for i := 0; i < 3; i++ {
		burstyLimiter <- time.Now()
	}
	// 每200ms 向缓冲限制器 添加一个新的值
	go func() {
		for t := range time.Tick(200 * time.Millisecond) {
			burstyLimiter <- t
		}
	}()

	burstyRequests := make(chan int, 5)
	for i := 1; i <= 5; i++ {
		burstyRequests <- i
	}
	close(burstyRequests)
	for req := range burstyRequests{
		t := <-burstyLimiter // 消费 缓冲限制器中的消息
		fmt.Println("立即消费突发的请求:", t)
		fmt.Println("request", req, time.Now())
	}
}