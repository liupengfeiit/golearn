package main

import (
	"fmt"
	"time"
)

// Go 的select可以让你等待多个通道操作。将 goroutines 和 channel 与 select 相结合是 Go 的一个强大功能。
// 类似于多路复用
func main() {
	c1 := make(chan string)
	c2 := make(chan string)
	go func() {
		time.Sleep(1 * time.Second)
		c1 <- "one"
	}()

	go func() {
		time.Sleep(2 * time.Second)
		c2 <- "two"
	}()
	// 阻塞当前 goroutine, 如果所有的goroutine已经休眠或者执行结束，则panic报错
	// select {}
	for i := 0; i < 2; i++ {
		// 等待case的一次执行成功
		select {
		case msg1 := <-c1:
			fmt.Println("c1 received ", msg1)
		case msg2 := <-c2:
			fmt.Println("c2 received", msg2)
		case <-time.After(1 * time.Second):
			fmt.Println("超过1s")
		}
	}
}
