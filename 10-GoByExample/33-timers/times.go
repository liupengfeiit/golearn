package main

import (
	"fmt"
	"time"
)

// 定时器
// 我们经常想在未来的某个时间点执行 Go 代码，或者在某个时间间隔重复执行。Go 的内置 计时器和自动收报功能使这两项任务变得容易。
// 计时器代表未来的单个事件。你告诉定时器你想等多久，它提供了一个通道，到时候会收到通知。

func main() {
	timer1 := time.NewTimer(2 * time.Second)

	<-timer1.C
	fmt.Println("timer 1 fired")

	timer2 := time.NewTimer(time.Second)
	go func() {
		fmt.Println("goroutine begin")
		<- timer2.C
		fmt.Println("timer 2 fired")
	}()
	// Stop阻止Timer被触发。如果调用停止计时器，则返回true，如果计时器已经停止，则返回false
	stop2 := timer2.Stop()
	if stop2 {
		fmt.Println("timer 2 stopped")
	}
	time.Sleep(5 * time.Second)
}