package util

import "context"

type Lock interface {
	Lock(ctx context.Context, key string)
	Unlock(ctx context.Context, key string)
	TryLock(ctx context.Context)
}
