
## Protoc编译proto文件生成go代码

protoc order.proto --go_out=./

```shell
protoc order.proto --go_out=./
# 提示protoc-gen-go未安装
protoc-gen-go: program not found or is not executable
Please specify a program using absolute path or make sure the program is available in your PATH system variable
--go_out: protoc-gen-go: Plugin failed with status code 1.
```

```shell
# 安装protoc-gen-go  安装protobuf的golang插件
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```
The compiler plugin protoc-gen-go will be installed in $GOBIN, defaulting to $GOPATH/bin. It must be in your $PATH for the protocol compiler protoc to find it.

默认安装把目录在`$GOPATH/bin`目录下
```shell
# 查看GOPATH
echo $GOPATH
# 查看环境变量
cat ~/.bash_profile
```

```shell
# $SRC_DIR 源码目录,环境变量
# $DST_DIR 目标目录,环境变量
# protoc -I=$SRC_DIR --go_out=$DST_DIR $SRC_DIR/addressbook.proto
# 编译proto文件,输出为go文件
protoc --go_out=./ order.proto
```
