package main

import (
	"fmt"
	"math/rand"
	"time"
)

/* 多路复用 */

/*
通道信息合并
input1 ->
			c -> ...
input2 ->
*/

func fanIn(input1, input2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			select {
			case s := <-input1:
				c <- s
			case s := <-input2:
				c <- s

			}
		}
	}()
	return c
}

func boring(msg string) <-chan string { // 返回字符串的仅接收通道。
	c := make(chan string)
	go func() {
		for i := 0; ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(5)) * time.Second)
		}
	}()
	return c
}

func main() {
	c := fanIn(boring("Joe"), boring("Ann"))
	//整个会话超时
	// timeout := time.After(5 * time.Second)
	for {
		select {
		case s := <-c:
			fmt.Println(s)
		case <-time.After(5 * time.Second):
			fmt.Println("You're too slow.")
			return
			//case <-timeout:
			//	fmt.Println("You're too slow.")
			//	return
		}
	}
}
