package main

import (
	"fmt"
	"math/rand"
	"time"
)

/* 生成器：返回通道的函数 */

func boring(msg string) <-chan string { // 返回字符串的仅接收通道。
	c := make(chan string)
	go func() {
		for i := 0; ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(10)) * time.Second)
		}
	}()
	return c
}

func main() {
	c := boring("boring")
	for i := 0; i < 5; i++ {
		fmt.Printf("You say: %q \n", <-c)
	}
	fmt.Println("You're boring; I'm leaving.")
}
