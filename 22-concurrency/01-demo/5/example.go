package main

import (
	"fmt"
	"math/rand"
	"time"
)

func boring(msg string) <-chan string { // 返回字符串的仅接收通道。
	c := make(chan string)
	go func() {
		for i := 0; ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(10)) * time.Second)
		}
	}()
	return c
}

func main() {
	// 可以多次生成通道
	joe := boring("Joe")
	ann := boring("Ann")
	for i := 0; i < 5; i++ {
		fmt.Println(<-joe)
		fmt.Println(<-ann)
	}
	fmt.Println("You're boring; I'm leaving.")
}
