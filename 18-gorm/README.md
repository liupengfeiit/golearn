# GORM 框架使用

https://gorm.io/zh_CN/

## 安装

```shell
go get -u gorm.io/gorm

go get -u gorm.io/driver/mysql

```


## 01-hello
快速入门
```go
dsn := "root:123456@tcp(127.0.0.1:3306)dbname?charset=utf8mb4&parseTime=True&loc=local"
```
想要正确的处理 time.Time ，您需要带上 parseTime 参数， (更多参数) 要支持完整的 UTF-8 编码，您需要将 charset=utf8 更改为 charset=utf8mb4 