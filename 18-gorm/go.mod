module gitee.com/liupengfei_it/golearn/18-gorm

go 1.16

require (
	gorm.io/gorm v1.22.5
	gorm.io/driver/mysql v1.2.3
)