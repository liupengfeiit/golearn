package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func main() {
	// dsn data source name
	dsn := "root:123456@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(fmt.Sprintf("connect db error %s", err.Error()))
	}
	// 迁移 schema
	db.AutoMigrate(&Product{})
	// create
	db.Create(&Product{Code: "hello", Price: 100})

	// read
	var product Product
	db.First(&product, 1)                   // 根据整型主键查找 	// 查询结果赋值给  &product指针
	db.First(&product, "code = ?", "hello") // select * from product where code = 'hello'
	db.Model(&product).Update("price", 200) // 修改价格

	db.Model(&product).Updates(Product{Price: 200, Code: "hello1"}) // 仅更新非零值字段

	db.Model(&product).Updates(map[string]interface{}{"Price": 200, "Code": "hello2"})

	// db.Delete(&product, 1) // 删除
	db.Model(&product).Delete(&product, 1)

}
