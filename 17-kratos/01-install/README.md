## Kratos 安装

### 1. go get 安装
```shell
go get -u github.com/go-kratos/kratos/cmd/kratos/v2@latest
```

### 2. go install 安装
```shell
go install github.com/go-kratos/kratos/cmd/kratos/v2
# go 1.16版本以上需要指定版本号或使用最新版
go install github.com/go-kratos/kratos/cmd/kratos/v2@latest
```

### 3. 源码编译安装
```shell
git clone https://github.com/go-kratos/kratos
cd kratos
make install
```