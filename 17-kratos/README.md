# Kratos

Kratos 一套轻量级 Go 微服务框架，包含大量微服务相关框架及工具。

[官方文档](https://go-kratos.dev/docs/)

[github地址](https://github.com/go-kratos/kratos)