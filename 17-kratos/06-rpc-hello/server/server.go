package main

import (
	"context"
	"fmt"
	"gitee.com/liupengfei_it/golearn/17-kratos/06-rpc-hello/helloworld"
	"github.com/go-kratos/kratos/v2/errors"
)

const name = "helloworld"

type server struct {
	helloworld.UnimplementedGreeterServer
}

func (s *server) SayHello(ctx context.Context, in *helloworld.HelloRequest) (*helloworld.HelloReply, error) {
	if in.Name == "error" {
		return nil, errors.BadRequest("custom_error", fmt.Sprintf("invalid argument %s", in.Name))
	}
	if in.Name == "panic" {
		panic("server panic")
	}
	return &helloworld.HelloReply{Message: fmt.Sprintf("hello %+v\n", in.Name)}, nil
}
