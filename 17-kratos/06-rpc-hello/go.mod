module gitee.com/liupengfei_it/golearn/17-kratos/06-rpc-hello

go 1.16

require (
	github.com/go-kratos/kratos/v2 v2.1.5
	google.golang.org/genproto v0.0.0-20220211171837-173942840c17
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
