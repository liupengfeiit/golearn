package service

import (
	"blog/internal/biz"
	"github.com/go-kratos/kratos/v2/log"
)

type UserService struct {
	uc  *biz.UserUsecase
	log *log.Helper
}

func NewUserService(uc *biz.UserUsecase, logger log.Logger) *UserService {
	return &UserService{
		uc:  uc,
		log: log.NewHelper(logger),
	}
}
