package data

import (
	"blog/internal/biz"
	"context"
	"github.com/go-kratos/kratos/v2/log"
)

type userRepo struct {
	data *Data
	log  *log.Helper
}

func NewUserRepo(data *Data, logger log.Logger) biz.UserRepo {
	return &userRepo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (userRepo *userRepo) CreateUser(ctx context.Context, user *biz.User) error {
	_, err := userRepo.data.db.User.Create().SetName(user.Name).SetAge(user.Age).Save(ctx)
	return err
}
