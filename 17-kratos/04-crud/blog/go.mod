module blog

go 1.16

require (
	entgo.io/ent v0.10.0
	github.com/go-kratos/kratos/v2 v2.1.4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/wire v0.5.0
	go.opentelemetry.io/otel v1.0.0
	go.opentelemetry.io/otel/trace v1.0.0
	google.golang.org/genproto v0.0.0-20211223182754-3ac035c7e7cb
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
