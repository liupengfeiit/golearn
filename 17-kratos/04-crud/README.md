## blog

简单的CRUD工程，包含MySQL和Redis的使用，展示使用kratos-layout创建的项目的完整结构


## ent
orm 框架
https://entgo.io/zh/docs/getting-started
```shell
# 安装
go get -d entgo.io/ent/cmd/ent

# 创建第一个表结构
cd internal/data
go run entgo.io/ent/cmd/ent init User

# 设置好表结构后
go generate ./ent
```