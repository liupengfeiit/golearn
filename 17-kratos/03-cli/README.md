
## CLI工具

### 安装
```shell
go install github.com/go-kratos/kratos/cmd/kratos/v2@latest
```


### 工具使用

```shell
# 查看版本
kratos -v
```

### 创建项目
```shell
kratos new helloworld

cd helloworld

go mod download
```

### 添加Proto文件
```shell
kratos proto add api/helloworld/demo.proto
```

### 生成Proto代码
```shell
# 通过make api 生成
make api

# 也可以通过kratos proto client 生成
kratos proto client api/helloworld/demo.proto

ls api/helloworld/

# api/helloworld/demo.pb.go
# api/helloworld/demo_grpc.pb.go
# 注意 http 代码只会在 proto 文件中声明了 http 时才会生成
# api/helloworld/demo_http.pb.go
```

### 生成Service代码
```shell
kratos proto server api/helloworld/demo.proto -t internal/service
```
