# Kratos 集成Gorm , CRUD

```shell
kratos new blog

cd blog
# 拉取项目依赖
go mod download
```

## 项目编译和运行

```shell
cd blog
# 生成所有proto源码,wire等
go generate ./...

# 运行项目
kratos run
```


## gorm
* go.mod
```
require (
    gorm.io/gorm v1.22.5
    gorm.io/driver/mysql v1.2.3
)
```


## 测试

* 添加book
```shell
curl -H "Context-Type:application/json" -X POST -d '{"name": "golang", "author": "谷歌", "num": 666, "price": 99.88, "weight": 0.125, "context": "golang 教程"}' 'http://localhost:8000/v1/book'

curl -H "Context-Type:application/json" -X POST -d '{"title": "golang", "content": "谷歌"}' 'http://localhost:8000/v1/article'

curl 'http://localhost:8000/v1/article'   -X POST

```

