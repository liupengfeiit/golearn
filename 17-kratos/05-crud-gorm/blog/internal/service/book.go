package service

import (
	v1 "blog/api/helloworld/v1"
	"blog/internal/biz"
	"context"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/jinzhu/copier"
)

type BookService struct {
	v1.UnimplementedBookServiceServer

	uc  *biz.BookUsecase
	log *log.Helper
}

func NewBookService(uc *biz.BookUsecase, logger log.Logger) *BookService {
	return &BookService{uc: uc, log: log.NewHelper(logger)}
}

func (service *BookService) Create(ctx context.Context, request *v1.CreateBookRequest) (*v1.CreateBookResponse, error) {
	service.log.Info(fmt.Sprintf("create book param: %+v\n", request))
	log.Info("create book start....")
	book := &biz.Book{}
	copier.Copy(book, request)
	service.log.Info(fmt.Sprintf("create book param: %+v\n", book))
	err := service.uc.Create(ctx, book)
	return &v1.CreateBookResponse{}, err
}
