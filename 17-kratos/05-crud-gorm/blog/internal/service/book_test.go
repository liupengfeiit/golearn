package service

import (
	v1 "blog/api/helloworld/v1"
	"blog/internal/biz"
	"fmt"
	"github.com/jinzhu/copier"
	"testing"
)

func TestCopy(t *testing.T) {
	request := &v1.CreateBookRequest{
		Name: "golang",
	}
	book := &biz.Book{}
	copier.Copy(book, request)

	fmt.Printf("book： %+v \n", book)
}
