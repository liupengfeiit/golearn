package biz

import (
	"context"
	"github.com/go-kratos/kratos/v2/log"
	"gorm.io/gorm"
)

type Book struct {
	gorm.Model
	Name    string
	Author  string
	Num     int32
	Price   float64
	Weight  float64
	Context string
}

type BookRepo interface {
	CreateBook(ctx context.Context, book *Book) error
}

type BookUsecase struct {
	repo BookRepo
	log  *log.Helper
}

func NewBookUsecase(repo BookRepo, logger log.Logger) *BookUsecase {
	return &BookUsecase{repo: repo, log: log.NewHelper(logger)}
}

func (uc *BookUsecase) Create(ctx context.Context, book *Book) error {
	return uc.repo.CreateBook(ctx, book)
}
