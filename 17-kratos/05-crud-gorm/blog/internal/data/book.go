package data

import (
	"blog/internal/biz"
	"context"
	"github.com/go-kratos/kratos/v2/log"
)

type bookRepo struct {
	data *Data
	log  *log.Helper
}

func NewBookRepo(data *Data, logger log.Logger) biz.BookRepo {
	return &bookRepo{data: data, log: log.NewHelper(logger)}
}

func (repo *bookRepo) CreateBook(ctx context.Context, book *biz.Book) error {
	// 迁移 schema
	repo.data.db.AutoMigrate(&biz.Book{})
	// create
	repo.data.db.Create(book)
	return nil
}
