package data

import (
	"blog/internal/conf"
	"database/sql"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	_ "github.com/go-sql-driver/mysql"
	"github.com/google/wire"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"time"
)

// ProviderSet is data providers.
var ProviderSet = wire.NewSet(NewData, NewGreeterRepo, NewBookRepo)

// Data .
type Data struct {
	db *gorm.DB
}

// NewData .
func NewData(c *conf.Data, logger log.Logger) (*Data, func(), error) {
	log := log.NewHelper(logger)
	sqlDb, err := sql.Open(c.Database.Driver, c.Database.Source)
	if err != nil {
		log.Error(fmt.Sprintf("connect db error : %s", err.Error()))
		panic(err)
	}
	log.Info(fmt.Sprintf("load configs: %s", c.String()))
	sqlDb.SetMaxIdleConns(int(c.Database.MaxIdleConn))
	sqlDb.SetMaxOpenConns(int(c.Database.MaxOpenConn))
	sqlDb.SetConnMaxLifetime(time.Duration(c.Database.ConnMaxLifeTime) * time.Minute)
	gormDb, err := gorm.Open(mysql.New(mysql.Config{Conn: sqlDb}), &gorm.Config{})
	if err != nil {
		log.Error(fmt.Sprintf("gorm db warpper error : %s", err.Error()))
		panic(err)
	}
	cleanup := func() {
		log.Info("closing the data resources")
	}
	return &Data{db: gormDb}, cleanup, nil
}
